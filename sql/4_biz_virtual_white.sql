
-- ----------------------------
-- Table 白名单表
-- ----------------------------
DROP TABLE IF EXISTS `virtual_white`;
CREATE TABLE `virtual_white`  (
                                  `vw_id` int(1) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                  `mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机号',
                                  `addtime` int(1) NOT NULL DEFAULT 0 COMMENT '更新时间',
                                  PRIMARY KEY (`vw_id`) USING BTREE,
                                  INDEX `mobile`(`mobile`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '虚拟号段白名单信息表' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;