/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50548
 Source Host           : localhost:3306
 Source Schema         : market

 Target Server Type    : MySQL
 Target Server Version : 50548
 File Encoding         : 65001

 Date: 24/12/2021 18:57:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for lxx_stat
-- ----------------------------
DROP TABLE IF EXISTS `lxx_stat`;
CREATE TABLE `lxx_stat`  (
  `id` bigint(20) NOT NULL,
  `commodity_id` bigint(20) NULL DEFAULT NULL COMMENT '商品id',
  `commodity_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `order_count` bigint(20) NULL DEFAULT NULL COMMENT '统计数量',
  `stat_time` datetime NULL DEFAULT NULL COMMENT '统计时间',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
