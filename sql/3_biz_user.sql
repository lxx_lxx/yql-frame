-- ----------------------------
-- 用户表
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(1) NOT NULL AUTO_INCREMENT COMMENT '用户编号',
  `mobile` varchar(50) NOT NULL DEFAULT '' COMMENT '用户手机号',
  `openid` varchar(50) NOT NULL DEFAULT '' COMMENT '微信openid',
  `c_user_id` varchar(50) NOT NULL DEFAULT '' COMMENT '建行手机银行三要素-用户编号',
  `c_sign` varchar(300) NOT NULL DEFAULT '' COMMENT '建行手机银行三要素-加密字符串',
  `c_timestamp` varchar(50) NOT NULL DEFAULT '' COMMENT '建行手机银行三要素-时间戳',
  `add_time` int(1) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  PRIMARY KEY (`user_id`),
  KEY `mobile` (`mobile`),
  KEY `openid` (`openid`),
  KEY `c_user_id` (`c_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息表';