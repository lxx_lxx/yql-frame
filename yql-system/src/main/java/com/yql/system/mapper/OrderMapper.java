package com.yql.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yql.common.annotation.DataSource;
import com.yql.common.core.domain.entity.Commodity;
import com.yql.common.core.domain.entity.Order;
import com.yql.common.core.domain.request.OrderListReq;
import com.yql.common.core.domain.response.TopThreeCommodityRsp;
import com.yql.common.enums.DataSourceType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 商品表 数据持久层
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/23 12:00
 */
@Mapper
@DataSource(DataSourceType.MASTER)
public interface OrderMapper extends BaseMapper<Order> {

    /**
     * 条件分页查询订单列表
     *
     * @param req
     * @Date 2021/12/24 11:55
     * @Author longxiao
     */
    List<Order> list(OrderListReq req);

    /**
     * 订单销量排名前三的商品及订单数量
     *
     * @Date 2021/12/24 14:42
     * @Author longxiao
     */
    List<TopThreeCommodityRsp> topThreeCommodity(Long topNum);
}
