package com.yql.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yql.common.annotation.DataSource;
import com.yql.common.core.domain.entity.Commodity;
import com.yql.common.enums.DataSourceType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 商品表 数据持久层
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/23 12:00
 */
@Mapper
@DataSource(DataSourceType.MASTER)
public interface CommodityMapper extends BaseMapper<Commodity> {
    List<Commodity> list(Commodity commodity);
}
