package com.yql.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yql.common.annotation.DataSource;
import com.yql.common.core.domain.dto.StatCommodityListDto;
import com.yql.common.core.domain.entity.Stat;
import com.yql.common.core.domain.response.StatCommodityListRsp;
import com.yql.common.core.domain.po.StatReportPo;
import com.yql.common.enums.DataSourceType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 统计表 数据持久层
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/23 12:00
 */
@Mapper
@DataSource(DataSourceType.MASTER)
public interface StatMapper extends BaseMapper<Stat> {

    /**
     * 昨日每个商品的订单量
     *
     * @Date 2021/12/26 11:23
     * @Author longxiao
     */
    List<Stat> ytdCommodityCount();


    List<StatCommodityListRsp> list(StatCommodityListDto dto);

    /**
     * 前七天数据每日所有商品订单总量
     * @Date 2021/12/26 18:14
     * @Author longxiao
     */
    List<StatReportPo> report();

}
