package com.yql.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yql.common.core.domain.entity.Stat;
import com.yql.common.core.domain.request.StatCommodityListReq;
import com.yql.common.core.domain.response.StatCommodityListRsp;
import com.yql.common.core.domain.po.StatReportPo;
import com.yql.common.core.domain.response.StatReportRsp;

import java.util.List;

/**
 * 统计服务
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/24 9:16
 */
public interface IStatService extends IService<Stat> {

    /**
     * 昨日每个商品的订单量
     *
     * @Date 2021/12/26 11:23
     * @Author longxiao
     */
    List<Stat> ytdCommodityCount();

    /**
     * 根据统计的数据回显统计列表
     *
     * @param req
     * @return
     * @Date 2021/12/26 14:38
     * @Author longxiao
     */
    List<StatCommodityListRsp> selectList(StatCommodityListReq req);

    /**
     * 前七天数据每日所有商品订单总量
     * @Date 2021/12/26 18:14
     * @Author longxiao
     */
    StatReportRsp report();
}
