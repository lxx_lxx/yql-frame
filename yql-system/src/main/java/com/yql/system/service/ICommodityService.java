package com.yql.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yql.common.core.domain.entity.Commodity;

import java.util.List;

/**
 * 商品服务
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/23 11:59
 */
public interface ICommodityService extends IService<Commodity> {
    List<Commodity> selectList(Commodity commodity);
}
