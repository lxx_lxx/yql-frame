package com.yql.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yql.common.core.domain.entity.Order;
import com.yql.common.core.domain.request.OrderListReq;
import com.yql.common.core.domain.request.PlaceOrderReq;
import com.yql.common.core.domain.response.TopThreeCommodityRsp;

import java.util.List;

/**
 * 订单服务
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/24 9:16
 */
public interface IOrderService extends IService<Order> {

    /**
     * 下单
     *
     * @param req
     * @Date 2021/12/24 9:39
     * @Author longxiao
     */
    String placeOrder(PlaceOrderReq req);

    /**
     * 条件分页查询订单列表
     *
     * @param req
     * @Date 2021/12/24 11:55
     * @Author longxiao
     */
    List<Order> selectList(OrderListReq req);

    /**
     * 订单销量排名前三的商品及订单数量
     *
     * @Date 2021/12/24 14:42
     * @Author longxiao
     */
    List<TopThreeCommodityRsp> topThreeCommodity(Long topNum);
}
