package com.yql.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yql.common.core.domain.entity.Commodity;
import com.yql.system.mapper.CommodityMapper;
import com.yql.system.service.ICommodityService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品服务
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/23 11:59
 */
@Service
public class CommodityServiceImpl extends ServiceImpl<CommodityMapper, Commodity> implements ICommodityService{


    @Override
    public List<Commodity> selectList(Commodity commodity) {
        return baseMapper.list(commodity);
    }
}
