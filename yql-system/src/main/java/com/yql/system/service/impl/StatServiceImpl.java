package com.yql.system.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yql.common.core.domain.dto.StatCommodityListDto;
import com.yql.common.core.domain.entity.Stat;
import com.yql.common.core.domain.request.StatCommodityListReq;
import com.yql.common.core.domain.response.StatCommodityListRsp;
import com.yql.common.core.domain.po.StatReportPo;
import com.yql.common.core.domain.response.StatReportRsp;
import com.yql.common.utils.bean.LxxBeanUtil;
import com.yql.system.mapper.StatMapper;
import com.yql.system.service.IStatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 统计服务
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/23 11:59
 */
@Service
@Slf4j
public class StatServiceImpl extends ServiceImpl<StatMapper, Stat> implements IStatService {

    /**
     * 昨日每个商品的订单量
     *
     * @Date 2021/12/26 11:23
     * @Author longxiao
     */
    @Override
    public List<Stat> ytdCommodityCount() {
        return baseMapper.ytdCommodityCount();
    }


    /**
     * 根据统计的数据回显统计列表
     * 默认回显昨日的每个商品的订单总量，
     * 也可以根据时间范围或具体某一天来显示每个商品的订单总量
     *
     * @param req
     * @return
     * @Date 2021/12/26 14:38
     * @Author longxiao
     */
    @Override
    public List<StatCommodityListRsp> selectList(StatCommodityListReq req) {
        StatCommodityListDto dto = LxxBeanUtil.copyProperties(req, StatCommodityListDto::new);
        if (StrUtil.isBlank(req.getBeginTime()) && StrUtil.isBlank(req.getEndTime()) && StrUtil.isBlank(req.getDay())) {
            dto.setIsDefault("Y");
        } else {
            dto.setIsDefault("N");
        }
        log.info("dto {}", dto);
        return baseMapper.list(dto);
    }

    /**
     * 前七天数据每日所有商品订单总量
     *
     * @Date 2021/12/26 18:14
     * @Author longxiao
     */
    @Override
    public StatReportRsp report() {
        List<StatReportPo> reports = baseMapper.report();
        log.info("reports {}", reports);
        List<String> days = new ArrayList<>();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DatePattern.NORM_DATE_PATTERN);
        LocalDate localDate = LocalDate.now().minusDays(7);
        IntStream.range(0,6).forEach(i -> days.add(localDate.plusDays(i).format(dtf)));

        log.info("days {}", days);
        List<Integer> orderCounts = new ArrayList<>();
        days.forEach(day -> {
            StatReportPo statReportPo = reports.stream().filter(report -> day.equals(report.getDay())).findAny().orElse(null);
            if (statReportPo == null) {
                orderCounts.add(0);
            } else {
                orderCounts.add(statReportPo.getOrderCount());
            }
        });
        log.info("days {},orderCounts {}", days, orderCounts);
        return new StatReportRsp(days, orderCounts);
    }
}
