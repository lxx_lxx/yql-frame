package com.yql.framework.interceptor.impl;

import javax.servlet.http.HttpServletRequest;

import com.yql.common.core.redis.RedisCache;
import com.yql.common.utils.StringUtils;
import com.yql.common.utils.security.Md5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.yql.common.json.JSON;
import com.yql.framework.interceptor.RepeatSubmitInterceptor;

/**
 * 判断请求url和数据是否和上一次相同， 
 * 如果和上次相同，则是重复提交表单。 有效时间为10秒内。
 * 
 * @author ruoyi
 */
@Component
public class SameUrlDataInterceptor extends RepeatSubmitInterceptor {


    @Autowired
    RedisCache redisCache;
    /**
     * 间隔时间，单位:秒 默认10秒
     * 
     * 两次相同参数的请求，如果间隔时间大于该参数，系统不会认定为重复提交的数据
     */
    private int intervalTime = 10;

    public void setIntervalTime(int intervalTime)
    {
        this.intervalTime = intervalTime;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean isRepeatSubmit(HttpServletRequest request, Long expireTime) throws Exception
    {
        // 本次参数及系统时间
        String nowParams = JSON.marshal(request.getParameterMap());
        String url = request.getRequestURI();
        String key = Md5Utils.hash(url+nowParams);
        if (StringUtils.isEmpty(key)){
            return false;
        }else {
            Boolean result = redisCache.getLock(key, expireTime);
            if (true == result){
                return false;
            }else {
                return true;
            }
        }
    }

}
