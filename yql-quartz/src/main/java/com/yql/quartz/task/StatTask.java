package com.yql.quartz.task;

import cn.hutool.core.collection.CollectionUtil;
import com.yql.common.core.domain.entity.Stat;
import com.yql.system.service.IStatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 统计定时任务
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/24 18:23
 */
@Component
@Slf4j
public class StatTask {

    @Resource
    private IStatService taskService;

    /**
     * 每日凌晨三点 统计昨日每个商品的订单量写入统计表中
     *
     * @Date 2021/12/26 10:06
     * @Author longxiao
     */
    public void statOrder() {
        log.info("开始执行定时任务=={====>每日凌晨三点 统计昨日每个商品的订单量写入统计表中");
        List<Stat> stats = taskService.ytdCommodityCount();
        if (CollectionUtil.isEmpty(stats)) {
            log.info("---昨日没有商品被下单---");
            return;
        }
        LocalDate yesterday = LocalDate.now().minus(1, ChronoUnit.DAYS);
        log.info("stat {}", stats);
        stats.forEach(stat -> stat.setStatTime(yesterday));
        taskService.saveBatch(stats);
    }
}
