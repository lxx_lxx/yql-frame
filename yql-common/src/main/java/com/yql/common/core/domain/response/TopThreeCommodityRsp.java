package com.yql.common.core.domain.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 排名前三商品 参数
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/24 14:38
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TopThreeCommodityRsp {

    /** 商品id */
    private Long id;

    /** 商品名称 */
    private String commodityName;

    /** 订单的数量 */
    private Long orderCount;
}
