package com.yql.common.core.domain.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.math3.analysis.function.Add;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * 添加商品请求参数
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/23 15:06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddCommodityReq {

    /**
     * 商品名称
     */
    @NotBlank(message = "请输入商品名称",groups = Add.class)
    private String commodityName;

    /**
     * 商品库存
     */
    private Long commodityNum;

    /**
     * 商品单价金额
     */
    private BigDecimal commodityAmount;

}
