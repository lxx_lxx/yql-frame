package com.yql.common.core.domain.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 商品统计列表 回显参数
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/26 13:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatCommodityListRsp {

    /**
     * 商品id
     */
    private Long commodityId;

    /**
     * 商品名称
     */
    private  String commodityName;

    /**
     * 统计的订单数量
     */
    private BigDecimal orderCount;

}
