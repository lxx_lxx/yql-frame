package com.yql.common.core.domain.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 商品统计列表 请求参数
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/26 13:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatCommodityListReq {

    /** 查询订单统计时间范围 开始时间 */
    private String beginTime;

    /** 查询订单统计时间范围 结束时间 */
    private String endTime;

    /** 查询某一天的订单统计*/
    private String day;
}
