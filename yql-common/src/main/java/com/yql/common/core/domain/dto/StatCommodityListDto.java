package com.yql.common.core.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 查询商品统计列表 参数传输
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/26 13:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatCommodityListDto {

    /** 查询订单统计时间范围 开始时间 */
    private String beginTime;

    /** 查询订单统计时间范围 结束时间 */
    private String endTime;

    /** 查询某一天的订单统计*/
    private String day;

    /** 默认查询标识 Y-是 默认回显昨日的每个商品的订单总量;N-否 */
    private String isDefault;
}
