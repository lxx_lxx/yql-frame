package com.yql.common.core.domain.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 编辑商品请求参数
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/23 15:06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EditCommodityReq {

    /**
     * 商品id
     */
    private Long id;

    /**
     * 商品名称
     */
    private String commodityName;

    /**
     * 商品库存
     */
    private Long commodityNum;

    /**
     * 商品单价金额
     */
    private BigDecimal commodityAmount;

    /**
     * 商品状态
     */
    private String commodityStatus;

}
