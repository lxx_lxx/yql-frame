package com.yql.common.core.domain.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 订单分页请求
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/24 9:35
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderListReq {

    /**
     * 商品id
     */
    private Long commodityId;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 订单状态：P-待付款;C-手动取消;TC-超时取消;S-支付成功;F-支付失败
     */
    private String orderStatus;

    /**
     * 退款状态:  N-未退款；S-退款成功；F-退款失败
     */
    private String refundStatus;

    /**
     * 付款时间范围查询 开始时间
     */
    private String beginTime;

    /**
     * 付款时间范围查询 结束时间
     */
    private String endTime;
}
