package com.yql.common.core.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 订单实体类
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/24 8:56
 */
@Data
@TableName("lxx_order")
public class Order {

    /**
     * 自增id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 订单编号
     */
    @TableField("order_no")
    private String orderNo;

    /**
     * 商品id
     */
    @TableField("commodity_id")
    private Long commodityId;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 订单金额
     */
    @TableField("order_amount")
    private BigDecimal orderAmount;

    /**
     * 下单的商品数量
     */
    @TableField("commodity_count")
    private Long commodityCount;

    /**
     * 订单状态：P-待付款;C-手动取消;TC-超时取消;S-支付成功;F-支付失败
     */
    @TableField("order_status")
    private String orderStatus;

    /**
     * 退款状态:  N-未退款；S-退款成功；F-退款失败
     */
    @TableField("refund_status")
    private String refundStatus;


    /**
     * 支付时间
     */
    @TableField("pay_time")
    private LocalDateTime payTime;

    /**
     * 删除标识:  Y:已删除 N:未删除
     */
    @TableField(value = "is_delete", fill = FieldFill.INSERT)
    private String isDelete;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;
}
