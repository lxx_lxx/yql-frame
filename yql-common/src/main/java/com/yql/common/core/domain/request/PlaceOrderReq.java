package com.yql.common.core.domain.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 下单请求
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/24 9:35
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlaceOrderReq {

    /**
     * 商品id
     */
    @NotNull(message = "请输入商品id")
    private Long commodityId;

    /**
     * 下单的商品数量
     */
    @NotNull(message = "下单的商品数量")
    private Long commodityCount;

}
