package com.yql.common.core.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 商品实体类
 */
@Data
@TableName("lxx_commodity")
public class Commodity {

    /**
     * 自增id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 商品名称
     */
    @TableField("commodity_name")
    private String commodityName;

    /**
     * 商品库存
     */
    @TableField("commodity_num")
    private Long commodityNum;

    /**
     * 商品单价金额
     */
    @TableField("commodity_amount")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal commodityAmount;

    /**
     * 商品状态：Y:上架 N:下架
     */
    @TableField("commodity_status")
    private String commodityStatus;

    /**
     * 删除标识:  Y:已删除 N:未删除
     */
    @TableField(value = "is_delete", fill = FieldFill.INSERT)
    private String isDelete;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;
}
