package com.yql.common.core.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 商品订单统计表
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/24 8:56
 */
@Data
@TableName("lxx_stat")
public class Stat {

    /**
     * 自增id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 商品id
     */
    @TableField("commodity_id")
    private Long commodityId;

    /**
     * 商品名称
     */
    @TableField("commodity_name")
    private  String commodityName;

    /**
     * 统计的订单数量
     */
    @TableField("order_count")
    private BigDecimal orderCount;

    /**
     * 统计时间
     */
    @TableField(value = "stat_time")
    private LocalDate statTime;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;
}
