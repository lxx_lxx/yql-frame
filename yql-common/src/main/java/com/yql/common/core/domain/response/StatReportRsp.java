package com.yql.common.core.domain.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 商品报表统计 回显柱状图参数
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/26 18:10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatReportRsp {

    /** 横坐标数据 日期 yyyy-MM-dd */
    private List<String> days;

    /** 纵坐标数据 商品订单数量 */
    private List<Integer> orderCounts;
}
