package com.yql.common.annotation;

import java.lang.annotation.*;

/** 
 * @description: 跳过jwt验证注解 
 * @param:  
 * @return:  
 * @author 
 */ 
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PassLogin {boolean required() default true;
}
