package com.yql.common.utils.bean;

import cn.hutool.core.bean.BeanUtil;
import com.yql.common.core.domain.entity.Commodity;
import com.yql.common.core.domain.entity.SysUser;

import java.util.function.Supplier;

public class LxxBeanUtil {
    public static void main(String[] args) {
        SysUser sysUser = new SysUser();
        sysUser.setUserId(111L);
        System.out.println(copyProperties(sysUser, Commodity::new));
    }

    public static <T> T copyProperties(Object source, Supplier<T> target){
        T obj = target.get();
        if (source == null){
            return obj;
        }
        BeanUtil.copyProperties(source, obj);
        return obj;
    }
}
