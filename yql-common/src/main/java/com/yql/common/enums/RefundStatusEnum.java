package com.yql.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单状态枚举
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/24 10:49
 */
@Getter
@AllArgsConstructor
public enum RefundStatusEnum {

    N("N","未退款"),
    S("S","退款成功"),
    F("F","退款失败");

    private String code;
    private String desc;
}
