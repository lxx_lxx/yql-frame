package com.yql.common.enums;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

/**
 * 商品状态枚举
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/23 15:32
 */
@Getter
@AllArgsConstructor
public enum CommodityStatusEnum {

    Y("1","商品上架"),
    N("0","商品下架");

    private String code;
    private String desc;
}
