package com.yql.common.enums;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

/**
 * 逻辑删除标识
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/23 15:53
 */
@Getter
@AllArgsConstructor
public enum IsDeleteEnum {
    Y("Y","已删除"),
    N("N","未删除");

    private String code;
    private String desc;
}
