package com.yql.common.enums;

/**
 * 数据源
 * 
 * @author ruoyi
 */
public enum DataSourceType
{
    /**
     * admin系统管理库
     */
    MASTER,

    /**
     * business业务库1
     */
    SLAVE,

    /**
     * 分库分表
     */
    SHARDING,

    /**
     * 可扩展
     * business业务库2
     */
    XXX
}
