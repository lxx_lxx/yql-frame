package com.yql.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单状态枚举
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/24 10:49
 */
@Getter
@AllArgsConstructor
public enum OrderStatusEnum {

    P("P","待支付"),
    C("C","手动取消"),
    TC("TC","超时取消"),
    S("S","支付成功");

    private String code;
    private String desc;
}
