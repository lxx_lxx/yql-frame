package com.yql.commonextend.exception;

import com.yql.commonextend.response.ExceptionCode;

/**
 * @Author: huweixing
 * @ClassName: CommonException
 * @Description:
 * @Date: 2021-03-16
 * @Time: 11:50
 **/
public class CommonException extends BaseException {

    private String msg;

    public CommonException(ExceptionCode code) {
        super(code);
        this.msg = code.msg();
    }

    public CommonException(ExceptionCode code, Throwable e) {
        super(code, e);
    }

    public CommonException(ExceptionCode code, String msg, String... objects) {
        super(code);
        if (msg != null && !"".equals(msg) && objects != null && objects.length > 0) {
            String message = msg.replaceAll("\\{\\}", "%s");
            this.msg = String.format(message, objects);
        }
    }


    public CommonException(ExceptionCode code, Throwable e, String msg) {
        super(code, e);
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public CommonException args(String... args) {
        if (this.msg != null && !"".equals(this.msg) && args != null && args.length > 0) {
            this.msg = this.msg.replaceAll("\\{\\}", "%s");
            this.msg = String.format(this.msg, args);
        }

        return this;
    }

}
