package com.yql.commonextend.exception;

/**
 * @Author: chenjun
 * @Description: 加解密异常类
 * @Date: 2021-07-19
 */
public class CryptoException extends RuntimeException{

    public CryptoException(Throwable e) {
        super(e);
    }
}
