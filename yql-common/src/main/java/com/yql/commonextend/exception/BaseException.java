package com.yql.commonextend.exception;

import com.yql.commonextend.response.ExceptionCode;

/**
 * @Author: huweixing
 * @ClassName: BaseException
 * @Description: 通用异常基类
 * @Date: 2021-03-16
 * @Time: 11:49
 **/
public class BaseException extends RuntimeException {

    private int code;

    public BaseException(ExceptionCode code) {
        this(code.code(), code.msg());
    }

    public BaseException(ExceptionCode code, Throwable e) {
        this(code.code(), code.msg(), e);
    }

    BaseException(int code, String msg) {
        super(msg);
        this.code = code;
    }

    BaseException(int code, String msg, Throwable e) {
        super(e);
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public String toString() {
        return getClass().getSimpleName() + "-" + this.code;
    }
}
