package com.yql.commonextend.core.domin;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SmsEntity {

    private String appid;
    private String appKey;
    private String content;
    private String appIvs;
    private String mobile;
    private String sendUrl;
}
