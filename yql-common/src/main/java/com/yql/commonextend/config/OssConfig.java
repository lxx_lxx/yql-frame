package com.yql.commonextend.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "file.oss")
public class OssConfig {

    /** 项目名称 */
    private static String aliyunDomain;

    private static String aliyunPrefix;

    private static String aliyunEndPoint;

    private static String aliyunAccessKeyId;

    private static String aliyunAccessKeySecret;

    private static String aliyunBucketName;

    public static String getAliyunDomain() {
        return aliyunDomain;
    }

    public  void setAliyunDomain(String aliyunDomain) {
        OssConfig.aliyunDomain = aliyunDomain;
    }

    public static String getAliyunPrefix() {
        return aliyunPrefix;
    }

    public  void setAliyunPrefix(String aliyunPrefix) {
        OssConfig.aliyunPrefix = aliyunPrefix;
    }

    public static String getAliyunEndPoint() {
        return aliyunEndPoint;
    }

    public  void setAliyunEndPoint(String aliyunEndPoint) {
        OssConfig.aliyunEndPoint = aliyunEndPoint;
    }

    public static String getAliyunAccessKeyId() {
        return aliyunAccessKeyId;
    }

    public  void setAliyunAccessKeyId(String aliyunAccessKeyId) {
        OssConfig.aliyunAccessKeyId = aliyunAccessKeyId;
    }

    public static String getAliyunAccessKeySecret() {
        return aliyunAccessKeySecret;
    }

    public  void setAliyunAccessKeySecret(String aliyunAccessKeySecret) {
        OssConfig.aliyunAccessKeySecret = aliyunAccessKeySecret;
    }

    public static String getAliyunBucketName() {
        return aliyunBucketName;
    }

    public  void setAliyunBucketName(String aliyunBucketName) {
        OssConfig.aliyunBucketName = aliyunBucketName;
    }
}
