package com.yql.commonextend.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "file.ftp")
public class FTPConfig {

    private static String host;

    private static String username;

    private static String password;

    private static String basePath;

    private static String directory;

    private static Integer port;

    private static Boolean localSwitch;

    public static Boolean getLocalSwitch() {
        return localSwitch;
    }

    public  void setLocalSwitch(Boolean localSwitch) {
        FTPConfig.localSwitch = localSwitch;
    }

    public static String getHost() {
        return host;
    }

    public  void setHost(String host) {
        FTPConfig.host = host;
    }

    public static String getUsername() {
        return username;
    }

    public  void setUsername(String username) {
        FTPConfig.username = username;
    }

    public static String getPassword() {
        return password;
    }

    public  void setPassword(String password) {
        FTPConfig.password = password;
    }

    public static String getBasePath() {
        return basePath;
    }

    public  void setBasePath(String basePath) {
        FTPConfig.basePath = basePath;
    }

    public static String getDirectory() {
        return directory;
    }

    public  void setDirectory(String directory) {
        FTPConfig.directory = directory;
    }

    public static Integer getPort() {
        return port;
    }

    public  void setPort(Integer port) {
        FTPConfig.port = port;
    }
}
