package com.yql.commonextend.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author: huweixing
 * @ClassName: TripleDesCONFIG
 * @Description: 加密工具配置类
 * @Date: 2021-03-17
 * @Time: 11:29
 **/
@Component
@ConfigurationProperties(prefix = "triple.des")
public class TripleDesConfig {

    /**
     * 项目名称
     */
    private static String pubKey;

    /**
     * 版本
     */
    private static String pubIvs;

    public static String getPubKey() {
        return pubKey;
    }

    public void setPubKey(String pubKey) {
        TripleDesConfig.pubKey = pubKey;
    }

    public static String getPubIvs() {
        return pubIvs;
    }

    public void setPubIvs(String pubIvs) {
        TripleDesConfig.pubIvs = pubIvs;
    }
}
