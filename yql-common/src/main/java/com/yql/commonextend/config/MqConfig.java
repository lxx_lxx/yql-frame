package com.yql.commonextend.config;

//import com.aliyun.openservices.ons.api.PropertyKeyConst;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
    @ConfigurationProperties(prefix = "rocketmq")
    public class MqConfig {

    private static String accessKey;
    private static String secretKey;
    private static String nameSrvAddr;
    private static String instanceId;
    private static String sendMsgTimeoutMillis;

    private static String normalTopic;
    private static String normalGroupId;
    private static String orderTopic;
    private static String orderGroupId;
    private static String partOrderTopic;
    private static String partOrderGroupId;
    private static String transTopic;
    private static String transGroupId;

    private static String tagPrefix;


    public Properties getMqPropertie() {
        Properties properties = new Properties();
//        properties.setProperty(PropertyKeyConst.AccessKey, this.accessKey);
//        properties.setProperty(PropertyKeyConst.SecretKey, this.secretKey);
//        properties.setProperty(PropertyKeyConst.NAMESRV_ADDR, this.nameSrvAddr);
        return properties;
    }

    public static String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        MqConfig.accessKey = accessKey;
    }

    public static String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        MqConfig.secretKey = secretKey;
    }

    public static String getNameSrvAddr() {
        return nameSrvAddr;
    }

    public void setNameSrvAddr(String nameSrvAddr) {
        MqConfig.nameSrvAddr = nameSrvAddr;
    }

    public static String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        MqConfig.instanceId = instanceId;
    }

    public static String getSendMsgTimeoutMillis() {
        return sendMsgTimeoutMillis;
    }

    public void setSendMsgTimeoutMillis(String sendMsgTimeoutMillis) {
        MqConfig.sendMsgTimeoutMillis = sendMsgTimeoutMillis;
    }

    public static String getNormalTopic() {
        return normalTopic;
    }

    public void setNormalTopic(String normalTopic) {
        MqConfig.normalTopic = normalTopic;
    }

    public static String getNormalGroupId() {
        return normalGroupId;
    }

    public void setNormalGroupId(String normalGroupId) {
        MqConfig.normalGroupId = normalGroupId;
    }

    public static String getOrderTopic() {
        return orderTopic;
    }

    public void setOrderTopic(String orderTopic) {
        MqConfig.orderTopic = orderTopic;
    }

    public static String getOrderGroupId() {
        return orderGroupId;
    }

    public void setOrderGroupId(String orderGroupId) {
        MqConfig.orderGroupId = orderGroupId;
    }

    public static String getPartOrderTopic() {
        return partOrderTopic;
    }

    public void setPartOrderTopic(String partOrderTopic) {
        MqConfig.partOrderTopic = partOrderTopic;
    }

    public static String getPartOrderGroupId() {
        return partOrderGroupId;
    }

    public void setPartOrderGroupId(String partOrderGroupId) {
        MqConfig.partOrderGroupId = partOrderGroupId;
    }

    public static String getTransTopic() {
        return transTopic;
    }

    public void setTransTopic(String transTopic) {
        MqConfig.transTopic = transTopic;
    }

    public static String getTransGroupId() {
        return transGroupId;
    }

    public void setTransGroupId(String transGroupId) {
        MqConfig.transGroupId = transGroupId;
    }

    public static String getTagPrefix() {
        return tagPrefix;
    }

    public void setTagPrefix(String tagPrefix) {
        MqConfig.tagPrefix = tagPrefix;
    }
}
