package com.yql.commonextend.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "captcha.sms.plat")
public class SmsConfig {

    private  static String appKey;

    private  static String appId;

    private  static String appIv;

    private  static String template;

    public static String getAppKey() {
        return appKey;
    }

    public  void setAppKey(String appKey) {
        SmsConfig.appKey = appKey;
    }

    public static String getAppId() {
        return appId;
    }

    public  void setAppId(String appId) {
        SmsConfig.appId = appId;
    }

    public static String getAppIv() {
        return appIv;
    }

    public  void setAppIv(String appIv) {
        SmsConfig.appIv = appIv;
    }

    public static String getTemplate() {
        return template;
    }

    public  void setTemplate(String template) {
        SmsConfig.template = template;
    }
}
