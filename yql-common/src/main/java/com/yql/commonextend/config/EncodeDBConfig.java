package com.yql.commonextend.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author: huweixing
 * @ClassName: EncodeDBConfig
 * @Description: 数据保存db加密
 * @Date: 2021-05-10
 * @Time: 18:49
 **/
@Component
@ConfigurationProperties(prefix = "triple.db")
public class EncodeDBConfig {

    /**
     * 项目名称
     */
    private static String pubKey;

    /**
     * 版本
     */
    private static String pubIvs;

    public static String getPubKey() {
        return pubKey;
    }

    public void setPubKey(String pubKey) {
        EncodeDBConfig.pubKey = pubKey;
    }

    public static String getPubIvs() {
        return pubIvs;
    }

    public void setPubIvs(String pubIvs) {
        EncodeDBConfig.pubIvs = pubIvs;
    }

}
