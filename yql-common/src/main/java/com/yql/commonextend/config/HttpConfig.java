package com.yql.commonextend.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author: chenjun
 * @Description: XXX
 * @Date: 2021/8/4
 **/
@Component
@ConfigurationProperties(prefix = "service.http")
public class HttpConfig {

    private static String readTimeout = "10000";//读取数据超时时间

    private static String connectTimeout = "10000";//连接超时时间

    private static boolean isLocal = true;//是否本地调用

    private static String host = "";//代理ip

    private static String port = "";//代理端口

    public static String getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(String readTimeout) {
        HttpConfig.readTimeout = readTimeout;
    }

    public static String getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(String connectTimeout) {
        HttpConfig.connectTimeout = connectTimeout;
    }

    public static boolean getIsLocal() {
        return isLocal;
    }

    public void setIsLocal(boolean isLocal) {
        HttpConfig.isLocal = isLocal;
    }

    public static String getHost() {
        return host;
    }

    public void setHost(String host) {
        HttpConfig.host = host;
    }

    public static String getPort() {
        return port;
    }

    public void setPort(String port) {
        HttpConfig.port = port;
    }
}
