package com.yql.commonextend.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "service.uass")
public class UassConfig {


    //是否启用行内登录
    private static Boolean isUass;

    //uass项目名称
    private static String srcsys;

    //uass私钥、一般不修改
    private static String uassPRIVACY_KEY;

    //uass接口地址
    private static String uassurl;


    public static Boolean getIsUass() {
        return isUass;
    }

    public  void setIsUass(Boolean isUass) {
        UassConfig.isUass = isUass;
    }

    public static String getSrcsys() {
        return srcsys;
    }

    public  void setSrcsys(String srcsys) {
        UassConfig.srcsys = srcsys;
    }

    public static String getUassPRIVACY_KEY() {
        return uassPRIVACY_KEY;
    }

    public  void setUassPRIVACY_KEY(String uassPRIVACY_KEY) {
        UassConfig.uassPRIVACY_KEY = uassPRIVACY_KEY;
    }

    public static String getUassurl() {
        return uassurl;
    }

    public  void setUassurl(String uassurl) {
        UassConfig.uassurl = uassurl;
    }
}
