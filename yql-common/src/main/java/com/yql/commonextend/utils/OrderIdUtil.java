package com.yql.commonextend.utils;


import org.apache.commons.lang3.RandomStringUtils;


/**
 * @author Young
 * 生成唯一订单号
 */
public class OrderIdUtil {
    /**
     * 生成唯一订单号
     */
    public static String getOrderId() {
        String format = RandomStringUtils.randomNumeric(16);
        return "YQL" + format;
    }

    /**
     * 生成唯一订单号，可传入前缀
     */
    public static String getOrderId(String prefix) {
        String format = RandomStringUtils.randomNumeric(16);
        return prefix + format;
    }
}
