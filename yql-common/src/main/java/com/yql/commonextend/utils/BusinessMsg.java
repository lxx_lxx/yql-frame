package com.yql.commonextend.utils;

import lombok.Data;

@Data
public class BusinessMsg {
    private String code;

    private String message;

    private String data;
}
