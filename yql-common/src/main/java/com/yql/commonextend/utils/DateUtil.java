package com.yql.commonextend.utils;

import com.yql.common.utils.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @Author: chenjun
 * @Description: 日期处理
 * 集成ruoyi已有的DateUtils 不做改动
 * @Date: 2021-06-29
 */
public class DateUtil extends DateUtils {

    /**
     *
     * @param startDate 日期格式时间拼接上当天最小时间
     * @return 时间戳
     */
    public static String toStartTimeStr(String startDate){
        return getDateTimeMillis(DateUtils.parseDate(startDate+" "+"00:00:00")).toString();
    }

    /**
     *
     * @param endDate 日期格式时间拼接上当天最大时间
     * @return 时间戳
     */
    public static String toEndTimeStr(String endDate){
        return getDateTimeMillis(DateUtils.parseDate(endDate+" "+"23:59:59")).toString();
    }

    /**
     *
     * @param startDate 日期格式时间拼接上当天最小时间
     * @return 时间戳
     */
    public static Long toStartTimeLong(String startDate){
        return getDateTimeMillis(DateUtils.parseDate(startDate+" "+"00:00:00"));
    }

    /**
     *
     * @param endDate 日期格式时间拼接上当天最大时间
     * @return 时间戳
     */
    public static Long toEndTimeLong(String endDate){
        return getDateTimeMillis(DateUtils.parseDate(endDate+" "+"23:59:59"));
    }

    /**
     *
     * @param endDate 日期时间字符串，如2021-07-20 11:12:13
     * @return 时间戳字符串
     */
    public static String getDateTimeMillis(String endDate){
        return getDateTimeMillis(DateUtils.parseDate(endDate)).toString();
    }

    /**
     * 10位时间戳转换为指定日期格式
     *
     * @param second 时间戳
     * @param patten 格式
     * @return
     */
    public static String secondToDateStr(Long second, String patten) {
        if(null == second || second ==0){
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(second * 1000);//转换为毫秒
        Date date = calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat(patten);
        String dateString = format.format(date);
        return dateString;
    }

    /**
     * 10位时间戳转换为指定日期格式
     *
     * @param second 时间戳
     * @return
     */
    public static Date secondToDate(Long second){
        if(null == second || second ==0){
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(second * 1000);//转换为毫秒
        Date date = calendar.getTime();
        return date;
    }

    /**
     * 获取指定时间的 10 位时间戳
     *
     * @param date 时间
     * @return
     */
    public static Long getDateTimeMillis(Date date) {
        return date.getTime() / 1000;
    }

    /**
     * 当前时间向推几小时
     * @param ihour 小时
     * @param pattern 返回格式
     * @return String
     */
    public static String getBeforeByHourTime(int ihour, String pattern){
        String returnstr = "";
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - ihour);
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        returnstr = df.format(calendar.getTime());
        return returnstr;
    }

    /**
     * 获取当前时间毫秒数
     * @return
     */
    public static long getCurrTimeMilliSecond(){
        Date date = new Date();
        return date.getTime();
    }

    /**
     * 获取当前月的第一天的00:00:00时间戳
     * @return
     */
    public static long getMonthFirstDayTimestamp(){
        return getMonthFirstDayTimestamp(0);
    }

    /**
     * 获取当前月的最后一天的23:59:59的时间戳
     * @return
     */
    public static long getMonthLastDayTimestamp(){
        return getMonthLastDayTimestamp(0);
    }

    /**
     * 获取当前月的第一天的00:00:00时间戳
     * @param month N月
     * @return
     */
    public static long getMonthFirstDayTimestamp(int month){
        //获取当前月第一天
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar ca = Calendar.getInstance();
        ca.add(Calendar.MONTH, month);
        ca.set(Calendar.DAY_OF_MONTH, 1);//设置为1号,当前日期既为本月第一天
        String first = format.format(ca.getTime());
        return toStartTimeLong(first);
    }

    /**
     * 获取当前月的最后一天的23:59:59的时间戳
     * @param month N月
     * @return
     */
    public static long getMonthLastDayTimestamp(int month){
        //获取当前月最后一天
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar ca = Calendar.getInstance();
        ca.add(Calendar.MONTH, month);
        ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
        String last = format.format(ca.getTime());
        return toEndTimeLong(last);
    }
}
