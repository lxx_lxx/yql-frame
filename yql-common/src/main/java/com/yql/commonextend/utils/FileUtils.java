package com.yql.commonextend.utils;

import com.jcraft.jsch.SftpException;
import com.yql.common.config.RuoYiConfig;
import com.yql.common.exception.file.InvalidExtensionException;
import com.yql.common.utils.DateUtils;
import com.yql.common.utils.file.FileUploadUtils;
import com.yql.common.utils.file.MimeTypeUtils;
import com.yql.commonextend.config.FTPConfig;
import com.yql.commonextend.core.domin.ImageEntity;
import com.yql.commonextend.middleware.oss.CloudStorageService;
import com.yql.commonextend.middleware.oss.OSSFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;
import java.io.*;


public class FileUtils {
    private static Logger log = LoggerFactory.getLogger(FileUtils.class);

    /**
     * @description: 以默认配置进行文件上传
     * @param prefix 路径前缀 例如系统默认 D:/yql/uploadPath 加上前缀 /order 则路径更改为 D:/yql/uploadPath/order
     * @param file 上传的文件
     * @return:
     * @author
     */
    public static ImageEntity upload(String prefix, MultipartFile file) throws IOException, InvalidExtensionException, SftpException {
        //校验文件大小
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.DEFAULT_ALLOWED_EXTENSION);
        //上传
        String uploadType = RuoYiConfig.getUploadType();
        ImageEntity imageEntity = new ImageEntity();
        if (uploadType.equals("oss")) {
            String fileName = file.getOriginalFilename();
            String suffix = fileName.substring(fileName.lastIndexOf("."));
            CloudStorageService storage = OSSFactory.build();
            String url = storage.uploadPrefix(file.getBytes(), prefix, suffix);
            imageEntity.setOssUrl(url);
        }else if (uploadType.equals("local")){
            String url = FileUploadUtils.upload(RuoYiConfig.getProfile()+prefix, file);
            imageEntity.setLocalUrl(url);
        }else if (uploadType.equals("ftp")){
            String fileName = FileUploadUtils.extractFilename(file);
            if (FTPConfig.getLocalSwitch()){
                String url = FileUploadUtils.upload(RuoYiConfig.getProfile()+prefix, fileName,file);
                imageEntity.setLocalUrl(url);
                //TODO 本地的url 是否返回
            }
            String ftpUrl = ftpUpload(prefix, fileName, file);
            imageEntity.setFtpUrl(ftpUrl);
        }
        return imageEntity;
    }

    /**
     * @description: ftp上传
     * @param:
     * @return:
     * @author
     */
    private static String ftpUpload(String prefix, String fileName, MultipartFile file){
        FtpUtils ftp = new FtpUtils(FTPConfig.getUsername(), FTPConfig.getPassword(), FTPConfig.getHost(),FTPConfig.getPort());
        ftp.login();
        InputStream inputStream = null;
        String path = "";
        try {
            String directory = FTPConfig.getDirectory()+"/"+prefix+DateUtils.dateTime();
            inputStream = new ByteArrayInputStream(file.getBytes());
            ftp.upload(FTPConfig.getBasePath(), directory, fileName, inputStream);
            inputStream.close();
            path = FTPConfig.getBasePath()+directory+fileName;
        } catch (Exception e) {
            log.error("FileUtils==>ftpUpload==>",e);
        }finally {
            if (inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    log.error("FileUtils==>ftpUpload==>资源关闭异常",e);
                }
            }
            if (ftp != null){
                ftp.logout();
            }
        }
        return path;
    }







}
