package com.yql.commonextend.utils;

import java.security.SecureRandom;

public class UUIDUtil {

    public static String getUUidSecNo(){
        String r ;
        byte[] values = new byte[128];
        SecureRandom random = new SecureRandom();
        random.nextBytes(values);
        r = random.nextInt(9)+"";
        return r;
    }


    public static String getUUid(){
        String r ;
        byte[] values = new byte[128];
        SecureRandom random = new SecureRandom();
        random.nextBytes(values);
        r = random.nextInt()+""+random.nextInt()+random.nextInt()+random.nextInt(9)+random.nextInt(9) ;
        r = r.replaceAll("-", "");
        return r;
    }

    public static void main(String[] args) {
        System.out.println(getUUid());
    }
}
