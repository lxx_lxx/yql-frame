package com.yql.commonextend.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertiesUtil {
    private static Logger log = LoggerFactory.getLogger(PropertiesUtil.class);
    private static Map<String, Properties> cache = new HashMap<String, Properties>();

    public static String get(String configFileName, String keyword){
        return getProperties(configFileName).getProperty(keyword);
    }

    public static Properties getProperties(String configFileName){
        if (cache.get(configFileName) != null){
            return cache.get(configFileName);
        }
        InputStream is = PropertiesUtil.class.getClassLoader().getResourceAsStream(configFileName);
        Properties config = new Properties();
        try {
            config.load(is);
        } catch (IOException e) {
            log.error("获取properties失败=====>", e);
        }
        cache.put(configFileName, config);
        return config;
    }
}
