package com.yql.commonextend.utils;

public class RequestIdUtil {

    private static ThreadLocal<String> tLocal = new ThreadLocal<>();

    public static String getRequestId() {
        return tLocal.get();
    }


    public static void settLocal(String reqestId) {
        RequestIdUtil.tLocal.set(reqestId);
    }
}