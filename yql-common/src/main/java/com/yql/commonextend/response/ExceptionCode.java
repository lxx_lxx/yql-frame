package com.yql.commonextend.response;

/**
 * @Author: huweixing
 * @ClassName: ExceptionCode
 * @Description: 异常状态码
 * @Date: 2021-03-16
 * @Time: 11:45
 **/
public class ExceptionCode {

    private int code;

    private String msg;

    public ExceptionCode(int code) {
        this(code, null, new String[0]);
    }

    public ExceptionCode(int code, String msg, String... args) {
        this.code = code;
        this.msg = msg;
        args(args);
    }

    public int code() {
        return this.code;
    }

    public String msg() {
        return this.msg;
    }

    public ExceptionCode args(String... args) {
        if (this.msg != null && !"".equals(this.msg) && args != null && args.length > 0) {
            this.msg = this.msg.replaceAll("\\{\\}", "%s");
            this.msg = String.format(this.msg, (Object[]) args);
        }
        return this;
    }

}
