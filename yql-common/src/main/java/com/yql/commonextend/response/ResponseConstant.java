package com.yql.commonextend.response;

import io.swagger.models.auth.In;

/**
 * @Author: chenjun
 * @Description:通用返回编码
 * @Date: 2021-06-30
 **/
public class ResponseConstant {

    /**
     * 返回成功状态码
     */
    public static final Integer RESP_SUCC_CODE = 1000;
    public static final String RESP_SUCC_MESG = "处理成功";

    /**
     * 非法操作(权限类，非正常操作)
     */
    public static final Integer RESP_ERROR_CODE = 1200;
    public static final String RESP_ERROR_MESG = "非法操作";

    /**
    * 运行时异常或未捕获异常
    */
    public static final Integer RESP_RUNTIME_ERROR_CODE = 1300;
    public static final String RESP_RUNTIME_ERROR_MESG = "网络异常，请稍后再试";

    /**
     * 接口超时
     */
    public static final Integer RESP_TIMEOUT_ERROR_CODE = 1400;

    /**
     * 登录超时
     */
    public static final Integer RESP_LOGIN_TIMEOUT_ERROR_CODE = 1099;
    public static final String RESP_LOGIN_TIMEOUT_ERROR_MESG = "登录超时";

    /**
     * 未登录
     */
    public static final Integer RESP_NOT_LOGIN_ERROR_CODE = 1100;
    public static final String RESP_NOT_LOGIN_ERROR_MESG = "未登录";
}
