package com.yql.commonextend.response;

import cn.hutool.core.util.ObjectUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @chenjun
 * @Description: 通用返回模板
 * @Date: 2021-06-28
 **/
@Data
@ApiModel(value = "CommonResponse", description = "通用返回模板")
public class CommonResponse<T> implements Serializable {

    private static final long serialVersionUID = 3380370321354738645L;

    @ApiModelProperty(required = true, value = "状态码")
    private int code = 1000;

    @ApiModelProperty(required = true, value = "状态描述")
    private String msg;

    @ApiModelProperty(required = true, value = "业务数据")
    private T data;

    /*========================================================
                    获取返回的公共参数处理
    ========================================================*/
    public static <T> CommonResponse<T> successWithData(T data) {
        CommonResponse<T> cr = new CommonResponse<>();
        cr.setCode(ResponseConstant.RESP_SUCC_CODE);
        cr.setMsg(ResponseConstant.RESP_SUCC_MESG);
        cr.setData(data);
        return cr;
    }

    /**
     * 处理成功，但返回空对象或字符串
     */
    public static <T> CommonResponse<T> successWithNullObjOrStr() {
        return successWithData(null);
    }
    /**
     * 处理成功，但返回空list，前端会统一判断list[]
     */
    public static <T> CommonResponse<T> successWithNullList() {
        Map map = new HashMap<>();
        map.put("list", new ArrayList<>());
        CommonResponse cr = new CommonResponse<>();
        cr.setCode(ResponseConstant.RESP_SUCC_CODE);
        cr.setMsg(ResponseConstant.RESP_SUCC_MESG);
        cr.setData(map);
        return cr;
    }

    public static <T> CommonResponse<T> success() {
        CommonResponse<T> cr = new CommonResponse<>();
        cr.setCode(ResponseConstant.RESP_SUCC_CODE);
        cr.setMsg(ResponseConstant.RESP_SUCC_MESG);
        return cr;
    }

    public static <T> CommonResponse<T> success(T data) {
        CommonResponse<T> cr = new CommonResponse<>();
        cr.setCode(ResponseConstant.RESP_SUCC_CODE);
        cr.setMsg(ResponseConstant.RESP_SUCC_MESG);
        cr.setData(data);
        return cr;
    }

    public static <T> CommonResponse<T> successWithMsg(String msg) {
        CommonResponse<T> cr = new CommonResponse<>();
        cr.setCode(ResponseConstant.RESP_SUCC_CODE);
        cr.setMsg(msg);
        return cr;
    }

    public static <T> CommonResponse<T> failedWithMsg(String msg) {
        CommonResponse<T> cr = new CommonResponse<>();
        cr.setCode(ResponseConstant.RESP_RUNTIME_ERROR_CODE);
        cr.setMsg(msg);
        return cr;
    }

    public static <T> CommonResponse<T> failed() {
        CommonResponse<T> cr = new CommonResponse<>();
        cr.setCode(ResponseConstant.RESP_RUNTIME_ERROR_CODE);
        cr.setMsg(ResponseConstant.RESP_RUNTIME_ERROR_MESG);
        return cr;
    }

    public static <T> CommonResponse<T> RespWithCodeMsg(int code, String msg) {
        CommonResponse<T> cr = new CommonResponse<>();
        cr.setCode(code);
        cr.setMsg(msg);
        return cr;
    }

    public static <T> CommonResponse<T> RespWithCodeData(int code, T data) {
        CommonResponse<T> cr = new CommonResponse<>();
        cr.setCode(code);
        cr.setData(data);
        return cr;
    }
}
