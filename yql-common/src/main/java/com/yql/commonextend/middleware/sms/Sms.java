package com.yql.commonextend.middleware.sms;

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.yql.common.utils.DateUtils;
import com.yql.common.utils.StringUtils;
import com.yql.common.utils.security.Md5Utils;
import com.yql.commonextend.config.SmsConfig;
import com.yql.commonextend.core.domin.SmsEntity;
import com.yql.commonextend.utils.MapSortUtil;
import com.yql.commonextend.utils.OrderIdUtil;
import com.yql.commonextend.utils.TripleDesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class Sms {



    private static Logger log = LoggerFactory.getLogger(Sms.class);

    /**
     * 生成验证码并发送短信
     * @param mobile     短信参数对象
     * @return 0表示发送失败，成功=返回6位验证码
     */
    public  static String getMessage(String mobile)
    {

        log.info("-----------commonTools------------getMessage------------mobile={}", mobile);
        String code = String.valueOf((int)((Math.random()*9+1)*100000));
        if (StringUtils.isEmpty(code)) {
            code = "170170";
        }
        JSONObject object = sendMessage(mobile, code);
        if (object.getInt("code")==1000) {
            return code;
        }
        return "0";
    }

    private  static JSONObject sendMessage(String mobile, String code)
    {
        log.info("-----------commonTools------------sendMessage------------code={}", code);
        StringBuilder message = new StringBuilder();
        Map<String, String> map = new HashMap();
        Map<String, String> dataParam = new HashMap();
        map.put("appId", SmsConfig.getAppId());
        map.put("timestamp", DateUtils.getCurrTimeMillis().toString());
        dataParam.put("smsMobile", mobile);
        dataParam.put("smsContent", SmsConfig.getTemplate().replaceAll("\\{\\}", code));
        dataParam.put("orderNo", OrderIdUtil.getOrderId());
        TripleDesUtils tripleDesUtils = new TripleDesUtils();
        tripleDesUtils.setKey(SmsConfig.getAppKey().getBytes());
        tripleDesUtils.setIvs(SmsConfig.getAppIv().getBytes());
        String data = tripleDesUtils.TripleDesEncrypt(JSONUtil.toJsonStr(dataParam));
        map.put("data", data);
        Map<String, String> sortMap = MapSortUtil.sortMapByKeysASC(map);
        for (Map.Entry<String, String> entry : sortMap.entrySet()) {
            message.append((String)entry.getKey()).append("=").append((String)entry.getValue()).append("&");
        }
        String message1 = message.substring(0, message.length() - 1);
        message1 = message1 + SmsConfig.getAppKey();
        String sign = Md5Utils.hash(message1);
        map.put("sign", sign);
        log.info("-----commonTools-------sendMessage--------------SmsSendUrl={}=param={}", "http://114.55.93.94:1701/api/sms/smsSend", JSONUtil.toJsonStr(map));
        String result = HttpRequest.post("http://114.55.93.94:1701/api/sms/smsSend").body(JSONUtil.toJsonStr(map)).execute().body();
        log.info("-----commonTools-------sendMessage--------------result={}", result);
        return JSONUtil.parseObj(result);
    }

    /**
     * 发送短信
     * @param mobile,msg     短信参数对象
     * @return JSONObject
     */
    public static JSONObject sendMessageContext(String mobile, String msg)
    {
        log.info("-----------commonTools------------sendMessageContext------------code={}", msg);
        StringBuilder message = new StringBuilder();
        Map<String, String> map = new HashMap();
        Map<String, String> dataParam = new HashMap();
        map.put("appId", SmsConfig.getAppId());
        map.put("timestamp", DateUtils.getCurrTimeMillis().toString());
        dataParam.put("smsMobile", mobile);
        dataParam.put("smsContent", msg);
        dataParam.put("orderNo", OrderIdUtil.getOrderId());
        TripleDesUtils tripleDesUtils = new TripleDesUtils();
        tripleDesUtils.setKey(SmsConfig.getAppKey().getBytes());
        tripleDesUtils.setIvs(SmsConfig.getAppIv().getBytes());
        String data = tripleDesUtils.TripleDesEncrypt(JSONUtil.toJsonStr(dataParam));
        map.put("data", data);
        Map<String, String> sortMap = MapSortUtil.sortMapByKeysASC(map);
        for (Map.Entry<String, String> entry : sortMap.entrySet()) {
            message.append((String)entry.getKey()).append("=").append((String)entry.getValue()).append("&");
        }
        String message1 = message.substring(0, message.length() - 1);
        message1 = message1 + SmsConfig.getAppKey();
        String sign = Md5Utils.hash(message1);
        map.put("sign", sign);
        log.info("-----commonTools-------sendMessage--------------SmsSendUrl={}=param={}", "http://114.55.93.94:1701/api/sms/smsSend", JSONUtil.toJsonStr(map));
        String result = HttpRequest.post("http://114.55.93.94:1701/api/sms/smsSend").body(JSONUtil.toJsonStr(map)).execute().body();
        log.info("-----commonTools-------sendMessage--------------result={}", result);
        return JSONUtil.parseObj(result);
    }


    /**
     * @description:
     * @param:
     * @return:  0 失败 1 成功
     * @author
     */
    public static Integer sendMessage(SmsEntity smsEntity){
        String appid = smsEntity.getAppid();
        String appKey = smsEntity.getAppKey();
        String content = smsEntity.getContent();
        String appIvs = smsEntity.getAppIvs();
        String sendUrl = smsEntity.getSendUrl();
        String mobile = smsEntity.getMobile();
        Map<String, String> map = new HashMap();
        Map<String, String> dataParam = new HashMap();
        StringBuilder message = new StringBuilder();
        map.put("appId", appid);
        map.put("timestamp", DateUtils.getCurrTimeMillis().toString());
        dataParam.put("smsMobile", mobile);
        dataParam.put("smsContent", content);
        dataParam.put("orderNo", OrderIdUtil.getOrderId());
        TripleDesUtils tripleDesUtils = new TripleDesUtils();
        tripleDesUtils.setKey(appKey.getBytes());
        tripleDesUtils.setIvs(appIvs.getBytes());
        String data = tripleDesUtils.TripleDesEncrypt(JSONUtil.toJsonStr(dataParam));
        map.put("data", data);
        Map<String, String> sortMap = MapSortUtil.sortMapByKeysASC(map);
        for (Map.Entry<String, String> entry : sortMap.entrySet()) {
            message.append((String)entry.getKey()).append("=").append((String)entry.getValue()).append("&");
        }
        String message1 = message.substring(0, message.length() - 1);
        message1 = message1 + appKey;
        String sign = Md5Utils.hash(message1);
        map.put("sign", sign);
        log.info("-----commonTools-------sendMessage--------------SmsSendUrl={}=param={}", sendUrl, JSONUtil.toJsonStr(map));
        String result = HttpRequest.post(sendUrl).body(JSONUtil.toJsonStr(map)).execute().body();
        log.info("-----commonTools-------sendMessage--------------result={}", result);
        Integer code = JSONUtil.parseObj(result).getInt("code");
        if (code == 1000){
            return 1;
        }
        return 0;
    }
}
