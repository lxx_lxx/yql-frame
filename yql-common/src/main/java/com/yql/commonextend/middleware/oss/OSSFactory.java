package com.yql.commonextend.middleware.oss;

/**
 * 文件上传Factory
 */
public final class OSSFactory
{

    public static CloudStorageService build()
    {
            return new AliyunCloudStorageService();
    }

}
