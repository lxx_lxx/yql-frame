package com.yql.commonextend.middleware.rocketmq;

//import com.aliyun.openservices.ons.api.Message;
//import com.aliyun.openservices.ons.api.ONSFactory;
//import com.aliyun.openservices.ons.api.Producer;
//import com.aliyun.openservices.ons.api.SendResult;
import com.yql.commonextend.config.MqConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;
import java.util.Properties;

/**
 * @Author: chenjun
 * @Description: 普通消息消费者
 * @Date: 2021/7/22
 **/
public class MQNormalConsumer {

    private static Logger logger = LoggerFactory.getLogger(MQNormalConsumer.class);


//    private void sendMsg(String msgContent, String tagName, String MessageKey, Map<String, String> customPropertiesMap){
//        Properties properties = new MQProperties().getMQProperties();
//        Producer producer = ONSFactory.createProducer(properties);
//        // 在发送消息前，必须调用start方法来启动Producer，只需调用一次即可。
//        producer.start();
//        Message msg = new Message(
//                // 普通消息所属的Topic，切勿使用普通消息的Topic来收发其他类型的消息。
//                MqConfig.getNormalTopic(),
//                // Message Tag可理解为Gmail中的标签，对消息进行再归类，方便Consumer指定过滤条件在消息队列RocketMQ版的服务器过滤。
//                tagName,
//                // Message Body可以是任何二进制形式的数据，消息队列RocketMQ版不做任何干预。
//                // 需要Producer与Consumer协商好一致的序列化和反序列化方式。
//                msgContent.getBytes());
//        // 设置代表消息的业务关键属性，请尽可能全局唯一。
//        // 以方便您在无法正常收到消息情况下，可通过消息队列RocketMQ版控制台查询消息并补发。
//        // 注意：不设置也不会影响消息正常收发。
//        //msg.setKey("ORDERID_" + i);
//
//        try {
//            SendResult sendResult = producer.send(msg);
//            // 同步发送消息，只要不抛异常就是成功。
//            if (sendResult != null) {
//                logger.info(new Date() + " Send mq message success. Topic is:" + msg.getTopic() + " msgId is: " + sendResult.getMessageId());
//            }
//        }
//        catch (Exception e) {
//            // 消息发送失败，需要进行重试处理，可重新发送这条消息或持久化这条数据进行补偿处理。
//            logger.error(new Date() + " Send mq message failed. Topic is:" + msg.getTopic(), e);
//        }
//        // 在应用退出前，销毁Producer对象。
//        // 注意：如果不销毁也没有问题。
//        producer.shutdown();
//    }
}
