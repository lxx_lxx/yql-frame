package com.yql.commonextend.middleware.rocketmq;

//import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.yql.commonextend.config.MqConfig;

import java.util.Properties;

/**
 * @Author: chenjun
 * @Description: 读取rockemtq连接配置
 * @Date: 2021/7/27
 **/
public class MQProperties {

    public Properties getMQProperties(){
        Properties properties = new Properties();
        // AccessKeyId 阿里云身份验证，在阿里云用户信息管理控制台获取。
//        properties.put(PropertyKeyConst.AccessKey, MqConfig.getAccessKey());
//        // AccessKeySecret 阿里云身份验证，在阿里云用户信息管理控制台获取。
//        properties.put(PropertyKeyConst.SecretKey, MqConfig.getSecretKey());
//        //设置发送超时时间，单位：毫秒。
//        properties.setProperty(PropertyKeyConst.SendMsgTimeoutMillis, MqConfig.getSendMsgTimeoutMillis());
//        // 设置TCP接入域名，进入消息队列RocketMQ版控制台实例详情页面的接入点区域查看。
//        properties.put(PropertyKeyConst.NAMESRV_ADDR, MqConfig.getNameSrvAddr());
        return properties;
    }

}
