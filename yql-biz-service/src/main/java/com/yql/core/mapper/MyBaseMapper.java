package com.yql.core.mapper;

/**
 * @Author: chenjun
 * @Description: mybatis plus基础类，方便扩展
 * @Date: 2021-07-20
 */
public interface MyBaseMapper<T> extends com.baomidou.mybatisplus.core.mapper.BaseMapper<T> {

}
