package com.yql.core.service.impl;

import com.yql.core.mapper.MyBaseMapper;
import com.yql.core.service.IMyService;
/**
 * @Author: chenjun
 * @Description: mybatis plus基础类，方便扩展
 * @Date: 2021-07-20
 */
public class MyServiceImpl<M extends MyBaseMapper<T>, T>
        extends com.baomidou.mybatisplus.extension.service.impl.ServiceImpl<M, T> implements IMyService<T> {

}
