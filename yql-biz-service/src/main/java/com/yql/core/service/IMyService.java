package com.yql.core.service;

/**
 * @Author: chenjun
 * @Description: mybatis plus基础类，方便扩展
 * @Date: 2021-07-20
 */
public interface IMyService<T> extends com.baomidou.mybatisplus.extension.service.IService<T> {

}
