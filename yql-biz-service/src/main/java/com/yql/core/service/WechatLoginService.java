package com.yql.core.service;

import com.yql.biz.vo.UserLoginVo;
import com.yql.commonextend.response.CommonResponse;

public interface WechatLoginService {
    /**
     * @description: 微信静默授权
     * @param:
     * @return:
     * @author
     */
    CommonResponse baseAuthorization(String code);

    /**
     * @description: 微信非静默授权
     * @param:
     * @return:
     * @author
     */
    CommonResponse userinfoAuthorization(String code);
    /**
     * @description: 微信开发平台获取用户信息（默认）
     * @param:
     * @return:
     * @author
     */
}
