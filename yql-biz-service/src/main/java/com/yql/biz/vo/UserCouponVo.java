package com.yql.biz.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: huweixing
 * @ClassName: UserCouponVo
 * @Description: 我的优惠券展示vo类
 * @Date: 2021-03-16
 * @Time: 17:47
 **/
@Data
public class UserCouponVo implements Serializable {

    private static final long serialVersionUID = -8170084206763830978L;
    /**
     * 用户优惠券ID
     */
    private Integer userCouponId;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 优惠券ID
     */
    private Integer couponId;

    /**
     * 过期时间
     */
    private Long expirationTime;

    /**
     * 优惠券使用状态 0-待使用、1-锁定、2-已使用 3-作废
     */
    private Integer couponState;

    /**
     * 优惠券对应商品大类类型
     */
    private Integer couponGtype;

    /**
     * 优惠券类型名称
     */
    private String couponGtName;

    private String couponTypeName;

    /**
     * 优惠券对应商品小类类型
     */
    private Integer couponMinType;

    private String couponMinTypeName;

    /**
     * 抵扣金额
     */
    private Long discountAmount;

    /**
     * 是否可叠加
     */
    private Integer isCover;

    /**
     * 优惠券说明
     */
    private String couponDesc;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 优惠券领取状态  0-未领取、1-已领取、2-已作废
     */
    private Integer couponStatus;

    /**
     * 优惠券使用金额限制
     */
    private Long couponAmount;

    /**
     * 优惠券地区ID
     */
    private Integer reginId;

    /**
     * 区域名称
     */
    private String regionName;

    /**
     * 是否可用
     */
    private Integer isUsable;

    private String expirationTimeStr;

    private String discountAmountStr;


}
