package com.yql.biz.vo;

import java.io.Serializable;

/**
 * @Author: huweixing
 * @ClassName: UserOrderVo
 * @Description: 用户订单VO类
 * @Date: 2021-03-16
 * @Time: 20:29
 **/
public class UserOrderDetailVo implements Serializable {

    private static final long serialVersionUID = 7800467411802731320L;

    /**
     * 订单ID
     */
    private Integer orderId;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 订单状态
     */
    private Integer orderStatus;
    private String orderStatusStr;

    /**
     * 商品ID
     */
    private Integer goodsId;

    /**
     * 商品面额
     */
    private Long amount;
    private String amountStr;

    /**
     * 商品售价
     */
    private Long realAmount;
    private String realAmountStr;


    /**
     * 卡券抵扣
     */
    private Long couponAmount;
    private String couponAmountStr;

    /**
     * 积分抵扣
     */
    private Long integralAmount;
    private String integralAmountStr;
    /**
     * 实付金额
     */
    private Long payAmount;
    private String payAmountStr;

    /**
     * 充值账号
     */
    private String account;

    /**
     * 下单时间
     */
    private Long addTime;


    private String addTimeStr;
    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品icon
     */
    private String gtIcon;

    /**
     * 商品 大类名称
     */
    private String maxName;

    /**
     * 商品小类名称
     */
    private String minName;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getRealAmount() {
        return realAmount;
    }

    public void setRealAmount(Long realAmount) {
        this.realAmount = realAmount;
    }

    public Long getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(Long couponAmount) {
        this.couponAmount = couponAmount;
    }

    public Long getIntegralAmount() {
        return integralAmount;
    }

    public void setIntegralAmount(Long integralAmount) {
        this.integralAmount = integralAmount;
    }

    public Long getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Long payAmount) {
        this.payAmount = payAmount;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Long getAddTime() {
        return addTime;
    }

    public void setAddTime(Long addTime) {
        this.addTime = addTime;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGtIcon() {
        return gtIcon;
    }

    public void setGtIcon(String gtIcon) {
        this.gtIcon = gtIcon;
    }

    public String getMaxName() {
        return maxName;
    }

    public void setMaxName(String maxName) {
        this.maxName = maxName;
    }

    public String getMinName() {
        return minName;
    }

    public void setMinName(String minName) {
        this.minName = minName;
    }

    public String getAddTimeStr() {
        return addTimeStr;
    }

    public void setAddTimeStr(String addTimeStr) {
        this.addTimeStr = addTimeStr;
    }

    public String getAmountStr() {
        return amountStr;
    }

    public void setAmountStr(String amountStr) {
        this.amountStr = amountStr;
    }

    public String getRealAmountStr() {
        return realAmountStr;
    }

    public void setRealAmountStr(String realAmountStr) {
        this.realAmountStr = realAmountStr;
    }

    public String getCouponAmountStr() {
        return couponAmountStr;
    }

    public void setCouponAmountStr(String couponAmountStr) {
        this.couponAmountStr = couponAmountStr;
    }

    public String getIntegralAmountStr() {
        return integralAmountStr;
    }

    public void setIntegralAmountStr(String integralAmountStr) {
        this.integralAmountStr = integralAmountStr;
    }

    public String getPayAmountStr() {
        return payAmountStr;
    }

    public void setPayAmountStr(String payAmountStr) {
        this.payAmountStr = payAmountStr;
    }

    public String getOrderStatusStr() {
        return orderStatusStr;
    }

    public void setOrderStatusStr(String orderStatusStr) {
        this.orderStatusStr = orderStatusStr;
    }
}
