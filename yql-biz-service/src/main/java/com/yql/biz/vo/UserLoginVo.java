package com.yql.biz.vo;

import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserLoginVo {

    private String openid;
    private String mobile;
    private String userName;
    private String avatar;
    private String accessToken;
    private String c_user_id;
    private String c_sign;
    private String c_timestamp;
    private String image_key;
    private String image_code;
    private String validate_code;

}
