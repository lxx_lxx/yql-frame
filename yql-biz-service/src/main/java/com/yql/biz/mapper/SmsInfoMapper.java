package com.yql.biz.mapper;

import com.yql.biz.domain.SmsInfo;
import com.yql.core.mapper.MyBaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SmsInfoMapper extends MyBaseMapper<SmsInfo> {

}
