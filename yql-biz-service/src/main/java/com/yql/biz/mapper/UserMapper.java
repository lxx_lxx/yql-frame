package com.yql.biz.mapper;

import com.yql.biz.domain.User;
import com.yql.core.mapper.MyBaseMapper;
import com.yql.common.annotation.DataSource;
import com.yql.common.enums.DataSourceType;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户Mapper
 * 需继承BaseMapper<T>基类，基类中有可直接使用的增删改查方法
 * @author chenjun
 * @since 2021-07-05
 */
@DataSource(DataSourceType.SLAVE)
@Mapper
public interface UserMapper extends MyBaseMapper<User> {

}
