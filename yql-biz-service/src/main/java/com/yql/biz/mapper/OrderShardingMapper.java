package com.yql.biz.mapper;

import com.yql.biz.domain.OrderSharding;
import com.yql.common.annotation.DataSource;
import com.yql.common.enums.DataSourceType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@DataSource(DataSourceType.SHARDING)
@Mapper
public interface OrderShardingMapper {

    Integer save(OrderSharding orderSharding);

    List<OrderSharding> selectOrderList();

    OrderSharding selectSysOrderById(Long id);

    List<OrderSharding> selectSysOrderByTime(@Param("startTime")Long startTime,@Param("endTime")Long endTime);
}
