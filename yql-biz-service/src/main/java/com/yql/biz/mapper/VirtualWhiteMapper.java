package com.yql.biz.mapper;

import com.yql.common.annotation.DataSource;
import com.yql.common.enums.DataSourceType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@DataSource(DataSourceType.SLAVE)
@Mapper
public interface VirtualWhiteMapper {

    Integer selectCountByMobile(@Param("mobile")String mobile);

}
