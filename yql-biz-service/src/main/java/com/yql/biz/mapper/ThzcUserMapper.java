package com.yql.biz.mapper;

import java.util.List;
import java.util.Map;

import com.yql.biz.domain.ThzcUser;
import com.yql.common.annotation.DataSource;
import com.yql.common.enums.DataSourceType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author: huweixing
 * @ClassName: ThzcUserMapper
 * @Description: 客户管理Mapper接口
 * @Date: 2021-03-11
 * @Time: 18:31
 **/
@DataSource(DataSourceType.SLAVE)
@Mapper
public interface ThzcUserMapper {

    /**
     * 查询客户管理
     *
     * @param userId 客户管理ID
     * @return 客户管理
     */
    public ThzcUser selectThzcUserById(Integer userId);

    /**
     * 查询客户管理列表
     *
     * @param thzcUser 客户管理
     * @return 客户管理集合
     */
    public List<ThzcUser> selectThzcUserList(ThzcUser thzcUser);

    /**
     * 新增客户管理
     *
     * @param thzcUser 客户管理
     * @return 结果
     */
    public int insertThzcUser(ThzcUser thzcUser);

    /**
     * 修改客户管理
     *
     * @param thzcUser 客户管理
     * @return 结果
     */
    public int updateThzcUser(ThzcUser thzcUser);

    /**
     * 删除客户管理
     *
     * @param userId 客户管理ID
     * @return 结果
     */
    public int deleteThzcUserById(Integer userId);

    /**
     * 批量删除客户管理
     *
     * @param userIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteThzcUserByIds(String[] userIds);

    /**
     * 根据用户ID或手机号查询用户信息
     *
     * @param userId 用户ID
     * @param mobile 用户手机号
     * @return
     */
    ThzcUser selectUserByIdAndMoblie(@Param("cUserId") String cUserId, @Param("userId") Integer userId, @Param("mobile") String mobile);

    /**
     * 根据用户手机号查询用户
     *
     * @param mobile
     * @return
     */
    ThzcUser selectUserByMoblie(String mobile);

    Long selectCountUser(@Param("beginTime") Long beginTime, @Param("endTime") Long endTime);

    Long selectCountOrder(@Param("beginTime") Long beginTime, @Param("endTime") Long endTime);

    List<Map> selectCharData();
}
