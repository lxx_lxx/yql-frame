package com.yql.biz.mapper;

import java.util.List;

import com.yql.biz.vo.UserCouponVo;
import com.yql.biz.domain.*;
import com.yql.common.annotation.DataSource;
import com.yql.common.enums.DataSourceType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author: huweixing
 * @ClassName: ThzcUserCouponMapper
 * @Description: 用户优惠券关联Mapper接口
 * @Date: 2021-03-16
 * @Time: 14:58
 **/
@DataSource(DataSourceType.SLAVE)
@Mapper
public interface ThzcUserCouponMapper {

    /**
     * 查询用户优惠券关联
     *
     * @param userCouponId 用户优惠券关联ID
     * @return 用户优惠券关联
     */
    public ThzcUserCoupon selectThzcUserCouponById(Integer userCouponId);

    /**
     * 查询用户优惠券关联列表
     *
     * @param thzcUserCoupon 用户优惠券关联
     * @return 用户优惠券关联集合
     */
    public List<ThzcUserCoupon> selectThzcUserCouponList(ThzcUserCoupon thzcUserCoupon);

    /**
     * 新增用户优惠券关联
     *
     * @param thzcUserCoupon 用户优惠券关联
     * @return 结果
     */
    public int insertThzcUserCoupon(ThzcUserCoupon thzcUserCoupon);

    /**
     * 修改用户优惠券关联
     *
     * @param thzcUserCoupon 用户优惠券关联
     * @return 结果
     */
    public int updateThzcUserCoupon(ThzcUserCoupon thzcUserCoupon);

    /**
     * 删除用户优惠券关联
     *
     * @param userCouponId 用户优惠券关联ID
     * @return 结果
     */
    public int deleteThzcUserCouponById(Integer userCouponId);

    /**
     * 批量删除用户优惠券关联
     *
     * @param userCouponIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteThzcUserCouponByIds(String[] userCouponIds);

    /**
     * 根据用户ID查询优惠券类表
     *
     * @param userId
     * @return
     */
    List<UserCouponVo> selectCouponVoByUserId(Integer userId);

    /**
     * 根据用户ID查询优惠券类表
     *
     * @param userId
     * @return
     */
    List<ThzcUserCoupon> selectCouponByUserId(Integer userId);

    /**
     * 统计可用优惠券数量
     *
     * @param userId      用户ID
     * @param orderAmount 订单金额
     * @param couponState 优惠券使用状态
     * @param regionArry  地区ID集合
     * @return
     */
    List<UserCouponVo> selectCountCouponByUserId(@Param("userId") Integer userId, @Param("orderAmount") Long orderAmount, @Param("couponState") Integer couponState, @Param("regionArry") Integer[] regionArry,@Param("expirationTime") Long expirationTime);


    /**
     * 根据优惠券ID集合 查询优惠券集合
     *
     * @param couponArry
     * @return
     */
    List<UserCouponVo> selectCouponByCId(@Param("couponArry") String[] couponArry);

    /**
     * 批量更新用户标签状态
     *
     * @param userCouponIds 用户标签关联ID
     * @param status        状态
     * @return
     */
    int updateBatchUserCouponStatus(@Param("userCouponIds") List<Integer> userCouponIds, @Param("status") Integer status);

    /**
     * 根据用户优惠券ID批量修改用户ID
     *
     * @param userCouponIds 用户优惠券ID
     * @param userId        用户ID
     * @return
     */
    int updateBatchUserCouponUserId(@Param("userCouponIds") List<Integer> userCouponIds, @Param("userId") Integer userId);

    /**
     * 绑定优惠券成功查询优惠券信息
     *
     * @param userCouponId 用户优惠券ID
     * @return
     */
    UserCouponVo selectUserCouponById(Integer userCouponId);

    /**
     * 根据订单ID查询用户优惠券列表
     *
     * @param orderId 订单ID
     * @return
     */
    List<ThzcUserCoupon> selectCountCouponByOrderId(Integer orderId);

    /**
     * @Author laisy
     * @Description 批量保存用户优惠券
     * @Date 14:26 2021/4/2
     * @Param [thzcUserCoupons]
     * @return void
     **/
    void insertBatchThzcUserCoupon(List<ThzcUserCoupon> thzcUserCoupons);

    /**
     * @Author laisy
     * @Description 查询用户在哪些活动领取的优惠券
     * @Date 16:10 2021/4/6
     * @Param [userId,couponType]
     * @return java.util.List<com.thzc.business.domain.ThzcUserCoupon>
     **/
    List<ThzcUserCoupon> selectIsUsbleCouponByUserId(@Param("userId") Integer userId,@Param("couponType") Integer couponType);
}
