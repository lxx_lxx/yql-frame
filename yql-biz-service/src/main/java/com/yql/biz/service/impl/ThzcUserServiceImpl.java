package com.yql.biz.service.impl;

import com.yql.biz.domain.ThzcUser;
import com.yql.biz.mapper.ThzcUserMapper;
import com.yql.biz.service.IThzcUserService;
import com.yql.common.annotation.DataSource;
import com.yql.common.constants.AppConstants;
import com.yql.common.core.redis.RedisCache;
import com.yql.common.enums.DataSourceType;
import com.yql.common.utils.StringUtils;
import com.yql.commonextend.config.EncodeDBConfig;
import com.yql.commonextend.utils.DateUtil;
import com.yql.commonextend.utils.Triple3DesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Author: huweixing
 * @ClassName: ThzcUserServiceImpl
 * @Description: 客户管理Service 接口实现类
 * @Date: 2021-03-11
 * @Time: 18:31
 **/
@DataSource(DataSourceType.SLAVE)
@Service
public class ThzcUserServiceImpl implements IThzcUserService{


    private static final Logger log = LoggerFactory.getLogger(ThzcUserServiceImpl.class);

    private static final Integer MESSAGE_LENGTH = 6;
    private static final String MESSAGE = "MESSAGE";
    public static final String UTF8 = "utf-8";

    private String oldUrl;

    @Resource
    private ThzcUserMapper thzcUserMapper;

    @Resource
    private RedisCache redisCache;

    /**
     * 查询客户管理
     *
     * @param userId 客户管理ID
     * @return 客户管理
     */
    public ThzcUser selectThzcUserById(Integer userId) {
        ThzcUser thzcUser = thzcUserMapper.selectThzcUserById(userId);
        return thzcUser;
    }

    public List<ThzcUser> selectThzcUserList(ThzcUser thzcUser) {
        if (!StringUtils.isEmpty(thzcUser.getMobile())) {
            Triple3DesUtils utils = new Triple3DesUtils(EncodeDBConfig.getPubKey(), EncodeDBConfig.getPubIvs());
            thzcUser.setMobile(utils.TripleDesEncrypt(thzcUser.getMobile(), utils.UTF8NAME));
        }
        //创建时间
        if (!StringUtils.isEmpty(thzcUser.getAddTimeStr()) && !StringUtils.isEmpty(thzcUser.getEndAddTimeStr())) {
            thzcUser.setAddTimeStr(DateUtil.toStartTimeStr(thzcUser.getAddTimeStr()));//开始
            thzcUser.setEndAddTimeStr(DateUtil.toEndTimeStr(thzcUser.getEndAddTimeStr())); //结束
        }

        List<ThzcUser> userList = thzcUserMapper.selectThzcUserList(thzcUser);
        userList.forEach(user -> {
            user.setAddTimeStr(DateUtil.secondToDateStr(user.getAddTime(), DateUtil.YYYY_MM_DD_HH_MM_SS));
            user.setEditTimeStr(DateUtil.secondToDateStr(user.getEditTime(), DateUtil.YYYY_MM_DD_HH_MM_SS));
            if (StringUtils.isNotEmpty(user.getMobile())) {
                Triple3DesUtils utils = new Triple3DesUtils(EncodeDBConfig.getPubKey(), EncodeDBConfig.getPubIvs());
                user.setMobile(utils.TripleDesDecrypt(user.getMobile(), utils.UTF8NAME));
            }
        });
        return userList;
    }

    public int insertThzcUser(ThzcUser thzcUser) {
        return 0;
    }

    public int updateThzcUser(ThzcUser thzcUser) {
        return 0;
    }

    public int deleteThzcUserByIds(String ids) {
        return 0;
    }

    public int deleteThzcUserById(Integer userId) {
        return 0;
    }

    public int save(String mobile, String cUserId) {
        return 0;
    }

    public ThzcUser selectUserByIdAndMoblie(String param, Integer userId){
        return null;
    }

    public Boolean bindMobile(Integer userId, String mobile, String captcha) {
        return null;
    }

    public Boolean getMessage(String mobile) {
        return null;
    }

    public String getMobileLocal(String mobile) {
        return null;
    }

    public Map<String, Object> getCount() {
        return null;
    }

    public List<Map> getCharData() {
        return null;
    }

}
