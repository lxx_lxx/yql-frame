package com.yql.biz.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wf.captcha.SpecCaptcha;
import com.wf.captcha.base.Captcha;
import com.yql.base.bankSms.BankSms;
import com.yql.base.utils.SocketUtils;
import com.yql.biz.common.GlobalConstants;
import com.yql.biz.domain.User;
import com.yql.biz.mapper.UserMapper;
import com.yql.biz.service.IUserService;
import com.yql.biz.vo.CaptchaResultVo;
import com.yql.biz.vo.UserLoginVo;
import com.yql.common.ExceptionCodes;
import com.yql.common.annotation.DataSource;
import com.yql.common.config.JWTConfig;
import com.yql.common.core.redis.RedisCache;
import com.yql.common.enums.DataSourceType;
import com.yql.common.utils.DateUtils;
import com.yql.common.utils.JwtUtils;
import com.yql.common.utils.StringUtils;
import com.yql.commonextend.exception.BusinessException;
import com.yql.commonextend.exception.CommonException;
import com.yql.commonextend.middleware.sms.Sms;
import com.yql.commonextend.response.CommonResponse;
import com.yql.commonextend.response.ResponseConstant;
import com.yql.commonextend.utils.RequestIdUtil;
import com.yql.core.service.impl.MyServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.awt.*;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 用户Mapper
 * 需继承ServiceImpl<BaseMapper<T>,T>基类
 * @author chenjun
 * @since 2021-07-05
 */
@DataSource(DataSourceType.SLAVE)
@Service
public class UserServiceImpl extends MyServiceImpl<UserMapper, User> implements IUserService {

    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    private static final long ONE_MONTH = (long) 24 * 60 * 60 * 30;
    @Resource
    private UserMapper userMapper;

    @Value("${captcha.sms.limitTimes}")
    private Integer SmsLimitTimes;

    @Value("${captcha.sms.isBankSms}")
    private Boolean useBankSms;

    @Value("${captcha.image.isUse}")
    private Boolean useImage;

    @Value("${captcha.image.width}")
    private Integer imageW;

    @Value("${captcha.image.height}")
    private Integer imageH;

    @Value("${captcha.image.digit}")
    private Integer digit;

    @Value("${captcha.sms.bank.fpayHost}")
    private String fpayHost;

    @Value("${captcha.sms.bank.fpayPort}")
    private String fpayPort;


    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BankSms bankSms;

    @Autowired
    private SocketUtils socketUtils;

    private static final long EXPIRE_3_MIN = 180L;

    public List<User> list(String mobile ,String userId){
        QueryWrapper wrapper= new QueryWrapper<>();
        if (StringUtils.isNotEmpty(mobile)){
//            if(!user.getMobile().startsWith("139")){//测试业务异常
//                throw new BusinessException(ExceptionCodes.getExceptionStr(ExceptionCodes.USER_1017));
//            }
            wrapper.eq("mobile",mobile);
        }
        if (ObjectUtil.isNotEmpty(userId)){
            wrapper.eq("user_Id",userId);
        }
        Page<User> page = new Page<>(1, 2);
        IPage<User> userIPage = userMapper.selectPage(page, wrapper);
        return userIPage.getRecords();
    }



    @Override
    public String forward() throws Exception {
        Class a = Class.forName("com.yql.biz.controller.LoginController");
        String simpleName = a.getSimpleName();
        String firstLowerName = simpleName.substring(0,1).toLowerCase()
                + simpleName.substring(1);
        //通过此方法去Spring容器中获取Bean实例
        Method m = a.getMethod("forwardTest",  new Class[]{String.class});
        Object i = m.invoke(SpringUtil.getBean(a), new Object[]{"123456"});
        return String.valueOf(i);
    }





}
