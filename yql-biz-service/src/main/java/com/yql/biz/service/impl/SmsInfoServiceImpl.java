package com.yql.biz.service.impl;

import com.yql.biz.domain.SmsInfo;
import com.yql.biz.mapper.SmsInfoMapper;
import com.yql.biz.service.SmsInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.TimerTask;

@Service
public class SmsInfoServiceImpl implements SmsInfoService {

    @Autowired
    SmsInfoMapper smsInfoMapper;

    @Async
    @Override
    public TimerTask saveSmsInfo(SmsInfo smsInfo) {
        return  new TimerTask() {
            @Override
            public void run() {
                smsInfoMapper.insert(smsInfo);
            }
        };
    }
}
