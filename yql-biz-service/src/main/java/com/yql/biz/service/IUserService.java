package com.yql.biz.service;

import com.yql.biz.domain.User;
import com.yql.biz.vo.UserLoginVo;
import com.yql.commonextend.response.CommonResponse;
import com.yql.core.service.IMyService;

import java.util.List;

public interface IUserService extends IMyService<User> {

    List<User> list(String mobile ,String userId);


    String forward() throws Exception;
}
