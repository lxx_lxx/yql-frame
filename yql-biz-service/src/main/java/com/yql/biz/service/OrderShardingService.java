package com.yql.biz.service;

import com.yql.biz.domain.OrderSharding;

import java.util.List;

public interface OrderShardingService {
    Integer save();

    List<OrderSharding> selectList();

    List<OrderSharding> selectListByTime();

    OrderSharding selectOrderSharding();
}
