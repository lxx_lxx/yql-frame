package com.yql.biz.service.impl;

import com.yql.biz.domain.OrderSharding;
import com.yql.biz.mapper.OrderShardingMapper;
import com.yql.biz.mapper.UserMapper;
import com.yql.biz.service.OrderShardingService;
import com.yql.common.annotation.DataSource;
import com.yql.common.enums.DataSourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@DataSource(DataSourceType.SHARDING)
@Service
public class OrderShardingServiceImpl implements OrderShardingService {

    @Autowired
    OrderShardingMapper orderShardingMapper;

    @Autowired
    UserMapper userMapper;

    @Override
    public Integer save() {
        OrderSharding orderSharding = new OrderSharding();
        orderSharding.setOrderNo("112");
        orderSharding.setStatus(1);
        orderSharding.setUserId(1);
        return  orderShardingMapper.save(orderSharding);
    }

    @Override
    public List<OrderSharding> selectList() {
        List<OrderSharding> list = orderShardingMapper.selectOrderList();
        return list;
    }

    @Override
    public List<OrderSharding> selectListByTime() {

        return orderShardingMapper.selectSysOrderByTime(1628654420L, 1628827220L);
    }

    @Override
    public OrderSharding selectOrderSharding() {

        return orderShardingMapper.selectSysOrderById(621280067380576257L);
    }
}
