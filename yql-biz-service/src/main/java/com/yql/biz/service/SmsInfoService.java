package com.yql.biz.service;

import com.yql.biz.domain.SmsInfo;

import java.util.TimerTask;

public interface SmsInfoService {

    TimerTask saveSmsInfo(SmsInfo smsInfo);

}
