package com.yql.biz.service;

import com.yql.biz.vo.UserLoginVo;
import com.yql.commonextend.response.CommonResponse;

public interface LoginService {

    void sendMsg(String mobile, String imageKey, String imageCode);

    CommonResponse getUserToken(UserLoginVo userLoginVo);

    CommonResponse doLogin(UserLoginVo userLoginVo);

    CommonResponse getImageCode();

}
