package com.yql.biz.service;

import com.yql.biz.domain.ThzcUser;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * @Author: huweixing
 * @ClassName: ThzcUserService
 * @Description: 客户管理Service接口
 * @Date: 2021-03-11
 * @Time: 18:31
 **/
public interface IThzcUserService {

    /**
     * 查询客户管理
     *
     * @param userId 客户管理ID
     * @return 客户管理
     */
    public ThzcUser selectThzcUserById(Integer userId);

    /**
     * 查询客户管理列表
     *
     * @param thzcUser 客户管理
     * @return 客户管理集合
     */
    public List<ThzcUser> selectThzcUserList(ThzcUser thzcUser);

    /**
     * 新增客户管理
     *
     * @param thzcUser 客户管理
     * @return 结果
     */
    public int insertThzcUser(ThzcUser thzcUser);

    /**
     * 修改客户管理
     *
     * @param thzcUser 客户管理
     * @return 结果
     */
    public int updateThzcUser(ThzcUser thzcUser);

    /**
     * 批量删除客户管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteThzcUserByIds(String ids);

    /**
     * 删除客户管理信息
     *
     * @param userId 客户管理ID
     * @return 结果
     */
    public int deleteThzcUserById(Integer userId);

    /**
     * 保存用户信息
     *
     * @param mobile  手机号
     * @param cUserId 银行标识
     * @return
     */
    int save(String mobile, String cUserId);

    /**
     * 根据用户ID或手机号查询用户信息
     *
     * @param param  用户银行信息
     * @param userId 用户ID
     * @return
     */
    ThzcUser selectUserByIdAndMoblie(String param, Integer userId);

    /**
     * 绑定手机号
     *
     * @param userId  用户ID
     * @param mobile  手机号
     * @param captcha 短信验证码
     * @return
     */
    Boolean bindMobile(Integer userId, String mobile, String captcha);

    /**
     * 用户获取验证码
     *
     * @param mobile 手机号
     * @return
     */
    Boolean getMessage(String mobile);

    /**
     * 获取手机号码归属地
     *
     * @param mobile 手机号
     * @return
     */
    String getMobileLocal(String mobile);

    /**
     * 首页数据统计
     *
     * @return
     */
    Map<String, Object> getCount();

    /**
     * 首页图表数据
     *
     * @return
     */
    List<Map> getCharData();
}
