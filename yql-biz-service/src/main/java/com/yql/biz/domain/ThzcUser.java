package com.yql.biz.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yql.common.annotation.Excel;
import com.yql.core.domain.MyBaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @Author: huweixing
 * @ClassName: ThzcUser
 * @Description: 用户实体 thzc_user
 * @Date: 2021-03-12
 * @Time: 15:06
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ThzcUser extends MyBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private Integer userId;

    /**
     * 手机号
     */
    @Excel(name = "手机号")
    private String mobile;

    /**
     * 用户银行标识
     */
    @Excel(name = "用户银行标识")
    private String cUserId;

    /**
     * 更新时间
     */
    private Long editTime;

    private String token;

    /**
     * 更新时间
     */
    @Excel(name = "更新时间")
    private String editTimeStr;

    /**
     * 更新时间，结束时间
     */
    private String endEditTimeStr;

    /**
     * 创建时间
     */

    private Long addTime;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    private String addTimeStr;

    /**
     * 创建时间,结束时间
     */
    private String endAddTimeStr;

    private String regionId;

    @Excel(name = "区域")
    private String regionName;

    private String oldUrl;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("userId", getUserId())
                .append("mobile", getMobile())
                .append("cUserId", this.getCUserId())
                .append("editTime", getEditTime())
                .append("addTime", getAddTime())
                .toString();
    }
}
