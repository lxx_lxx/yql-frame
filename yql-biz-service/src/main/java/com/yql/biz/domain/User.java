package com.yql.biz.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yql.core.domain.MyBaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户表
 * 需继承Mode<T>
 * @author chenjun
 * @since 2021-07-05
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("user")
public class User extends MyBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;


    /**
     * 手机号
     */
    private String mobile;

    /**
     * 手机银行三要素-用户编号
     */
    private String cUserId;

    /**
     * 手机银行三要素-时间戳
     */
    private String cTimestamp;

    /**
     * 手机银行三要素-加密字符串
     */
    private String cSign;


    /**
     * 礼品发放，用户openid（官微）
     */
    private String openid;

    /**
     * 创建时间
     */
    private Long createTime;


    /**
     * 创建时间
     */
    private Long addTime;
}
