package com.yql.biz.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder//若加builder注解 以下两个注解也要加上
@NoArgsConstructor
@AllArgsConstructor
public class OrderSharding {
    private Long orderId;
    private String orderNo;
    private Integer userId;
    private Integer status;
    private Long addTime;
}
