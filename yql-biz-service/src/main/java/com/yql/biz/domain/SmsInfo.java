package com.yql.biz.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder//若加builder注解 以下两个注解也要加上
@NoArgsConstructor
@AllArgsConstructor
@TableName("sms_info")
public class SmsInfo {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String mobile;

    private String content;

    private Integer status;

    private Long createTime;
}
