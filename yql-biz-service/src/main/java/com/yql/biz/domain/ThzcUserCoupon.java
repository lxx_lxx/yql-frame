package com.yql.biz.domain;

import lombok.Data;
import com.yql.common.annotation.Excel;
import com.yql.common.core.domain.BaseEntity;

/**
 * @Description: 用户优惠券关联实体 thzc_user_coupon
 * @Date: 2021-03-16
 * @Time: 14:58
 **/
@Data
public class ThzcUserCoupon extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 优惠券状态 0-待使用、1-锁定、2-已使用 3-作废
     */
    public static final Integer STATUS_0 = 0;
    public static final Integer STATUS_1 = 1;
    public static final Integer STATUS_2 = 2;
    public static final Integer STATUS_3 = 3;

    /**
     * 用户券关联表id
     */
    private Integer userCouponId;

    /**
     * 用户id
     */
    @Excel(name = "用户id")
    private Integer userId;

    /**
     * 优惠券id
     */
    @Excel(name = "优惠券id")
    private Integer couponId;

    /**
     * 领取时间
     */
    @Excel(name = "领取时间")
    private Long receiveTime;

    /**
     * 过期时间
     */
    @Excel(name = "过期时间")
    private Long expirationTime;

    /**
     * 优惠券使用状态 0-待使用、1-锁定、2-已使用 3-作废
     */
    @Excel(name = "优惠券使用状态")
    private Integer couponState;

    /**
     * 活动id
     */
    private Integer activityId;


}
