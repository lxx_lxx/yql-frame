package com.yql.biz.common;

public class GlobalConstants {


   public class captcha{
        public static final String SMS_CAPTCHA_PREFIX = "SMS:CAPTCHA:";
        //短信计数
        public static final String SMS_COUNT_PREFIX = "SMS:COUNT:";
        //用户短信验证码
        public static final String SMS_SEND_CODE = "SMS:CODE:";

   }

}
