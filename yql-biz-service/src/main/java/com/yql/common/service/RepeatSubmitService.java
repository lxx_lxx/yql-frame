package com.yql.common.service;

import com.yql.common.ExceptionCodes;
import com.yql.common.core.redis.RedisCache;
import com.yql.common.utils.StringUtils;
import com.yql.common.utils.security.Md5Utils;
import com.yql.commonextend.response.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RepeatSubmitService {

    @Autowired
    RedisCache redisCache;


    public CommonResponse verifyReSubmit(String mobile, Long expireTime ){
        log.info("mobile======>{},expireTime======>{}", mobile, expireTime);
        // 本次参数及系统时间
        if (null == mobile){
            CommonResponse commonResponse = CommonResponse.RespWithCodeMsg(ExceptionCodes.MSG_1075.code(), ExceptionCodes.MSG_1075.msg());
            return commonResponse;
        }
        String key = Md5Utils.hash(mobile);
        if (StringUtils.isEmpty(mobile)){
            CommonResponse commonResponse = CommonResponse.RespWithCodeMsg(ExceptionCodes.MSG_1075.code(), ExceptionCodes.MSG_1075.msg());
            return commonResponse;
        }else {
            Boolean result = redisCache.getLock(key, expireTime);
            log.info("result===>{}",result);
            if (result){
                CommonResponse commonResponse = CommonResponse.success();
                return commonResponse;
            }else {
                CommonResponse commonResponse = CommonResponse.RespWithCodeMsg(ExceptionCodes.MSG_1075.code(), ExceptionCodes.MSG_1075.msg());
                return commonResponse;
            }
        }
    }

}
