package com.yql.common.utils.logs;


public abstract class TraceContextUtil {

    /**
     * 日志跟踪
     */
    private static final ThreadLocal<String> TRACE_THREAD_LOCAL = new ThreadLocal<String>();

    public static void setTraceId(String traceId) {
        TRACE_THREAD_LOCAL.set(traceId);
    }

    public static String getTraceId() {
        return TRACE_THREAD_LOCAL.get();
    }

    public static void removeAll() {
        TRACE_THREAD_LOCAL.remove();
    }
}
