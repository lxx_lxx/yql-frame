package com.yql.common.constants;

/**
 * APP常量
 */
public class AppConstants {

    public static final String API_VERSION = "api_version";

    /**
     * 0-订单处理中 1-订单成功 2-订单失败
     */
    public static final Integer ORDER_STATE_0 = 0;
    public static final Integer ORDER_STATE_1 = 1;
    public static final Integer ORDER_STATE_2 = 2;
    public static final String UTF8 = "utf-8";
    //...........
}
