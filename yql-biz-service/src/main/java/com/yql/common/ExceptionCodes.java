package com.yql.common;

import com.yql.commonextend.response.ExceptionCode;

/**
 * @Author: huweixing
 * @ClassName: ExceptionCodes
 * @Description:
 * @Date: 2021-03-16
 * @Time: 11:47
 **/
public interface ExceptionCodes {

    ExceptionCode GOOD_1001 = new ExceptionCode(1001, "商品分类ID不能为空");
    ExceptionCode GOOD_1002 = new ExceptionCode(1002, "商品ID不能为空");
    ExceptionCode GOOD_1003 = new ExceptionCode(1003, "商品已下架或不存在");

    ExceptionCode USER_1011 = new ExceptionCode(1011, "用户信息不能为空");
    ExceptionCode USER_1012 = new ExceptionCode(1012, "系统无此用户信息");
    ExceptionCode USER_1013 = new ExceptionCode(1013, "用户手机号不能为空");
    ExceptionCode USER_1014 = new ExceptionCode(1014, "用户银行标识不能为空");
    ExceptionCode USER_1015 = new ExceptionCode(1015, "查询用户信息参数不能为空");
    ExceptionCode USER_1016 = new ExceptionCode(1016, "用户信息已存在");
    ExceptionCode USER_1017 = new ExceptionCode(1017, "请填写正确手机号");

    ExceptionCode COUPON_1020 = new ExceptionCode(1020, "优惠券编号不能为空");
    ExceptionCode COUPON_1021 = new ExceptionCode(1021, "无效优惠券");
    ExceptionCode COUPON_1022 = new ExceptionCode(1022, "重复优惠券");
    ExceptionCode COUPON_1023 = new ExceptionCode(1023, "优惠券[{}]类型不匹配");
    ExceptionCode COUPON_1024 = new ExceptionCode(1024, "[{}]不可叠加");
    ExceptionCode COUPON_1025 = new ExceptionCode(1025, "优惠券不是领取状态");
    ExceptionCode COUPON_1026 = new ExceptionCode(1026, "优惠券过期类型异常");
    ExceptionCode COUPON_1027 = new ExceptionCode(1026, "优惠券抵扣金额限制超过订单金额");

    ExceptionCode ACTIVITY_1030 = new ExceptionCode(1030, "无效活动信息");

    ExceptionCode ORDER_1040 = new ExceptionCode(1040, "订单详情参数不能为空");
    ExceptionCode ORDER_1041 = new ExceptionCode(1041, "充值账号不能为空");
    ExceptionCode ORDER_1042 = new ExceptionCode(1042, "订单金额不能小于零");
    ExceptionCode ORDER_1043 = new ExceptionCode(1043, "订单金额异常");
    ExceptionCode ORDER_1044 = new ExceptionCode(1044, "订单信息不存在");
    ExceptionCode ORDER_1045 = new ExceptionCode(1045, "订单金额不能为空");
    ExceptionCode ORDER_1046 = new ExceptionCode(1046, "操作频繁请稍后重试");
    ExceptionCode ORDER_1047 = new ExceptionCode(1047, "非常抱歉，当前交易金额超出当月限额，如有疑问请联系平台客服");

    ExceptionCode REGION_1050 = new ExceptionCode(1050, "地区信息不能为空");
    ExceptionCode REGION_1051 = new ExceptionCode(1051, "无效地区");

    ExceptionCode MSG_1060 = new ExceptionCode(1060, "短信下发失败");
    ExceptionCode MSG_1061 = new ExceptionCode(1061, "短信验证码不能为空");
    ExceptionCode MSG_1062 = new ExceptionCode(1062, "操作频繁，请稍后重试");
    ExceptionCode MSG_1063 = new ExceptionCode(1063, "图形验证码类型不能为空");
    ExceptionCode MSG_1064 = new ExceptionCode(1064, "短信验证码不正确");
    ExceptionCode MSG_1065 = new ExceptionCode(1065, "短信验证码使用上限，请联系管理员");
    ExceptionCode MSG_1066 = new ExceptionCode(1066, "图形验证码不正确");
    ExceptionCode MSG_1067 = new ExceptionCode(1067, "图形验证码过期");
    ExceptionCode MSG_1069 = new ExceptionCode(1069, "短信验证码过期");
    ExceptionCode MSG_1070 = new ExceptionCode(1070, "短信验证失败");

    ExceptionCode MSG_1068 = new ExceptionCode(1068, "非法参数");

    ExceptionCode MSG_1071 = new ExceptionCode(1071, "code不能为空");
    ExceptionCode MSG_1072 = new ExceptionCode(1072, "微信授权失败");

    ExceptionCode MSG_1073 = new ExceptionCode(1073, "暂不支持虚拟号段客户参与，请联系客服。");
    ExceptionCode MSG_1074 = new ExceptionCode(1073, "虚拟号码拦截异常");
    ExceptionCode MSG_1075 = new ExceptionCode(1075, "操作频繁，请稍后再试");

    static String getExceptionStr(ExceptionCode e){ //GlobalExceptionHandler统一处理
        return new StringBuilder().append(e.code()).append("&&").append(e.msg()).toString();
    }

}
