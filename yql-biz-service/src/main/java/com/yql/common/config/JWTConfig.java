package com.yql.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "jwt")
public class JWTConfig {
    private static String secret;

    public static String getSecret() {
        return secret;
    }

    public  void setSecret(String secret) {
        JWTConfig.secret = secret;
    }
}
