package com.yql.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "wx")
public class WxApiConfig {
    private static String appid;
    private static String secret;
    private static String redirect_url;


    public static String getAppid() {
        return appid;
    }

    public  void setAppid(String appid) {
        WxApiConfig.appid = appid;
    }

    public static String getSecret() {
        return secret;
    }

    public  void setSecret(String secret) {
        WxApiConfig.secret = secret;
    }

    public static String getRedirect_url() {
        return redirect_url;
    }

    public  void setRedirect_url(String redirect_url) {
        WxApiConfig.redirect_url = redirect_url;
    }
}
