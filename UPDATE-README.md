变更说明：
2021年12月07日
1.Sms类补充sendMessage方法，可发送短信（每个短信模板需要单独申请，且appid,appkey,appivs均不一样）
2.redis工具类补充判断缓存中是否存在key、队列批量入栈putAll方法
3.删除RocketMq相关jar包和注释相关代码，目前暂未使用，避免打的jar包过大

2021年9月27日
1.去掉代理接口WxProxy,替代接口为：HttpUtils

2021年9月16日
1、更新CommonController，去掉存在漏洞未使用方法，满足科技部安全整改要求
2、增加ParamValidAspect拦截器，满足科技部安全整改要求

2021年9月13日
1、后管admin代码修改登录密码采用DES前端加密、后端解密，满足科技部安全整改要求
2、后管admin密码复杂度改为大小写+数字+特殊字符，达到科技部安全整改要求

2021年8月15日
1、新增yql-commom的\com\yql\commonextend\utils\HttpUtils用于调用外部接口(含通过代理调用)，
  会单独打印日志到**out-interface.log文件中
2、redis工具类RedisCache增加自增、分布式锁、队列方法
  
2021年8月2日
1.将所有fastjson 改回 hutool包中的json
2.jwt改为存储user信息

2021年7月19日
1.api模块---新增虚拟号段拦截
2.api模块---增加登录controller(LoginController)
3.service模块---增加虚拟号段mapper相关接口
4.base模块增加银行短信接口,代理WxProxy接口
5.framework模块增加yql短信接口
6.api、admin 增加短信相关配置 前缀为captcha

2021年7月12日
api模块---新增sharding分库分表

2021年7月12日
增加行内员工登录开关
SysLoginService.login

2021年7月9日
api模块新增防重复提交功能 使用说明
api模块新增jwt验证功能










