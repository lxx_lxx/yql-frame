import com.yql.YqlApplication;
import com.yql.common.core.domain.request.StatCommodityListReq;
import com.yql.common.core.domain.response.StatCommodityListRsp;
import com.yql.system.service.IStatService;
import lombok.extern.slf4j.Slf4j;
import org.jasypt.encryption.StringEncryptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = YqlApplication.class)
@Slf4j
public class StatTest {

    @Resource
    private IStatService statService;

    @Test
    public void demo() {
        StatCommodityListReq req = new StatCommodityListReq();
        //req.setBeginTime("2021-12-23");
        //req.setEndTime("2021-12-24");
//        req.setDay("2021-12-25");
        List<StatCommodityListRsp> list = statService.selectList(req);
        log.info("list {}", list);
    }

}
