package com.yql.util;

import org.jasypt.encryption.StringEncryptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
@RunWith(SpringRunner.class)
@SpringBootTest
public class JasyptTest {

        private static final Logger log = LoggerFactory.getLogger(JasyptTest.class);
        @Autowired
        private StringEncryptor stringEncryptor;

        @Test
        public void encryptPwd() {
            //加密123456
            String result = stringEncryptor.encrypt("root");
            System.out.println(result);
            result = stringEncryptor.encrypt("iuyPKB#a");
            System.out.println(result);
        }

}
