/**
 * http 请求
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/26 17:45
 */
function lxxPost(url,data) {
    var resultData;
    var config = {
        url: url,
        type: "post",
        dataType: "json",
        data: data,
        async: false,
        beforeSend: function () {
            $.modal.loading("正在处理中，请稍后...");
        },
        success: function(result) {
            if (result.code === responseCode.SUCCESS) {
                $.modal.alertSuccess(result.msg)
            } else if (result.code === web_status.WARNING) {
                $.modal.alertWarning(result.msg)
            } else {
                $.modal.alertError(result.msg);
            }
            resultData = result;
            $.modal.closeLoading();
        }
    };
    $.ajax(config);
    return resultData;
}
