package com.yql.biz.controller;

import com.yql.common.annotation.PassLogin;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/biz")
public class BizController {

    @PassLogin
    @GetMapping("/test")
    public String testConnect(@RequestParam("id") String id){
        return id;
    }
}
