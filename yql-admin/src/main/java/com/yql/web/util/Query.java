package com.yql.web.util;

import java.util.List;

@FunctionalInterface
public interface Query<T> {

   List<T> callBack();
}
