package com.yql.web.util;

import com.github.pagehelper.PageInfo;
import com.yql.common.core.page.TableDataInfo;

import java.util.List;

public class PageResult<T> {

    public static <T> TableDataInfo page(Query<T> query){
        List<?> list = query.callBack();
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(0);
        rspData.setRows(list);
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }
}
