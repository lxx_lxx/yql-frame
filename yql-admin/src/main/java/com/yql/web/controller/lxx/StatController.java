package com.yql.web.controller.lxx;

import com.yql.biz.domain.User;
import com.yql.common.core.controller.BaseController;
import com.yql.common.core.domain.request.StatCommodityListReq;
import com.yql.common.core.domain.response.StatCommodityListRsp;
import com.yql.common.core.domain.po.StatReportPo;
import com.yql.common.core.domain.response.StatReportRsp;
import com.yql.common.utils.poi.ExcelUtil;
import com.yql.commonextend.response.CommonResponse;
import com.yql.system.service.IStatService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * 统计应用层
 *
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/26 12:53
 */
@Controller
@RequestMapping("/stat")
@Slf4j
public class StatController extends BaseController {

    private static String prefix = "/lxx/stat";

    @Resource
    private IStatService iStatService;

    @GetMapping("/go")
    public String go() {
        return prefix + "/stat";
    }

    @PostMapping("/list")
    @ResponseBody
    @ApiOperation("根据统计的数据回显统计列表")
    public CommonResponse<List<StatCommodityListRsp>> list(StatCommodityListReq req) {
        startPage();
        log.info("请求参数 req {}", req);
        return CommonResponse.success(iStatService.selectList(req));
    }

    @PostMapping("/report")
    @ResponseBody
    @ApiOperation("统计报表图表")
    public CommonResponse<StatReportRsp> report(String req) {
        log.info("请求参数 req {}", req);
        MDC.put("thread","11111111111");
        log.info("22222222222222222");
        ExcelUtil<User> userExcelUtil = new ExcelUtil<>(User.class);
        userExcelUtil.exportExcel();
        return CommonResponse.success(iStatService.report());
    }
}
