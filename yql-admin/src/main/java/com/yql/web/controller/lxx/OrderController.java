package com.yql.web.controller.lxx;

import cn.hutool.json.JSONUtil;
import com.yql.common.core.controller.BaseController;
import com.yql.common.core.domain.response.TopThreeCommodityRsp;
import com.yql.common.core.page.TableDataInfo;
import com.yql.commonextend.response.CommonResponse;
import com.yql.commonextend.utils.Triple3DesUtils;
import com.yql.common.core.domain.request.OrderListReq;
import com.yql.system.service.IOrderService;
import com.yql.common.core.domain.request.PlaceOrderReq;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author longxiaoxiao
 * @version 1.0
 * @date 2021/12/24 8:54
 */
@Controller
@RequestMapping("/order")
@Slf4j
public class OrderController extends BaseController {

    @Resource
    private IOrderService orderService;

    @PostMapping("/placeOrder")
    @ResponseBody
    @ApiOperation("用户商品下单接口")
    public String placeOrder(String encryptData) {
        String decJsonStr = new Triple3DesUtils().TripleDesDecrypt(encryptData, StandardCharsets.UTF_8.name());
        log.info("aes解密的Json字符串 {}", decJsonStr);
        PlaceOrderReq req = JSONUtil.toBean(decJsonStr, PlaceOrderReq.class);
        log.info("下单请求参数 {}", req);
        return orderService.placeOrder(req);
    }

    @PostMapping("/list")
    @ResponseBody
    @ApiOperation("用户订单分页查询接口")
    public TableDataInfo list(String encryptData) {
        log.info("encryptData {}", encryptData);
        String decJsonStr = new Triple3DesUtils().TripleDesDecrypt(encryptData, StandardCharsets.UTF_8.name());
        log.info("aes解密的Json字符串 {}", decJsonStr);
        OrderListReq req = JSONUtil.toBean(decJsonStr, OrderListReq.class);
        log.info("请求参数 {}", req);
        startPage();
        return getDataTable(orderService.selectList(req));
    }

    @GetMapping("/topThree")
    @ResponseBody
    @ApiOperation("订单销量排名前三的商品及订单数量")
    public CommonResponse<List<TopThreeCommodityRsp>> topThreeCommodity(@RequestParam("topNum") Long topNum) {
        return CommonResponse.success(orderService.topThreeCommodity(topNum));
    }

}
