package com.yql.web.controller.lxx;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.yql.common.annotation.Log;
import com.yql.common.core.controller.BaseController;
import com.yql.common.core.domain.AjaxResult;
import com.yql.common.core.domain.entity.Commodity;
import com.yql.common.core.page.TableDataInfo;
import com.yql.common.enums.BusinessType;
import com.yql.common.utils.poi.ExcelUtil;
import com.yql.system.service.ICommodityService;
import com.yql.common.enums.CommodityStatusEnum;
import com.yql.common.enums.IsDeleteEnum;
import com.yql.common.core.domain.request.AddCommodityReq;
import com.yql.common.core.domain.request.EditCommodityReq;
import com.yql.common.utils.bean.LxxBeanUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/commodity")
@Slf4j
public class CommodityController extends BaseController {

    private static String prefix = "/lxx/lxx";

    @Resource
    private ICommodityService commodityService;

    @GetMapping("/go")
    public String commodity() {
        return prefix + "/commodity";
    }

    @GetMapping("/go/add")
    public String goAdd() {
        return prefix + "/add";
    }

    @GetMapping("/go/edit/{id}")
    public String goEdit(@PathVariable("id") Long id, ModelMap map) {

        Commodity commodity = commodityService.getById(id);
        log.info("商品详情 {}", commodity);
        map.put("commodity", commodity);
        return prefix + "/edit";
    }

    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Commodity commodity) {
        log.info("请求参数 {}", commodity);
        startPage();
        return getDataTable(commodityService.selectList(commodity));
    }

    @PostMapping("/add")
    @ResponseBody
    public AjaxResult add(AddCommodityReq req) {
        log.info("请求参数 {}", req);
        Commodity lxxCommodity = LxxBeanUtil.copyProperties(req, Commodity::new);
        lxxCommodity.setCommodityStatus(CommodityStatusEnum.Y.getCode());
        return toAjax(commodityService.save(lxxCommodity));
    }

    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult edit(EditCommodityReq req) {
        log.info("请求参数 {}", req);
        Commodity lxxCommodity = LxxBeanUtil.copyProperties(req, Commodity::new);
        return toAjax(commodityService.updateById(lxxCommodity));
    }

    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult delete(String ids) {
        log.info("请求参数 {}", ids);
        String[] idsStr = ids.split(",");
        LambdaUpdateWrapper<Commodity> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(Commodity::getIsDelete, IsDeleteEnum.Y.getCode())
                .in(Commodity::getId, idsStr);
        return toAjax(commodityService.update(updateWrapper));
    }

    @Log(title = "商品管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Commodity commodity) {
        LambdaQueryWrapper<Commodity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.setEntity(commodity);
        List<Commodity> list = commodityService.list(queryWrapper);
        ExcelUtil<Commodity> util = new ExcelUtil<>(Commodity.class);
        return util.exportExcel(list, "商品数据");
    }
}
