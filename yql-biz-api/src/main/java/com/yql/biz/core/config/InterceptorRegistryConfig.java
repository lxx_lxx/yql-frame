package com.yql.biz.core.config;

import com.yql.biz.core.interceptor.LoginVerifyInterceptor;
import com.yql.biz.core.interceptor.RepeatSubmitApiInterceptor;
import com.yql.biz.core.interceptor.VirtualPhoneInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorRegistryConfig implements WebMvcConfigurer {
    @Autowired
    VirtualPhoneInterceptor virtualPhoneInterceptor;

    //@Autowired
    //RepeatSubmitApiInterceptor repeatSubmitApiInterceptor;

    //@Autowired
    //LoginVerifyInterceptor loginVerifyInterceptor;

    public void addInterceptors(InterceptorRegistry registry) {
        //拦截器按照注册先后顺序执行
        //registry.addInterceptor(repeatSubmitApiInterceptor).addPathPatterns("/**");
        registry.addInterceptor(virtualPhoneInterceptor).addPathPatterns("/**/doLogin");
        //registry.addInterceptor(loginVerifyInterceptor).addPathPatterns("/**");
    }
}
