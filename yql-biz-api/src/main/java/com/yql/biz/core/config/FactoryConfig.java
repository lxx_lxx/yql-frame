package com.yql.biz.core.config;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.yql.common.service.PassLoginService;
import com.yql.common.service.RepeatSubmitService;
import com.yql.common.annotation.PassLogin;
import com.yql.common.annotation.RepeatSubmitBiz;
import com.yql.commonextend.response.CommonResponse;
import com.yql.commonextend.utils.PropertiesUtil;
import net.sf.json.util.JSONUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Component
public class FactoryConfig {

    private static Logger log = LoggerFactory.getLogger(FactoryConfig.class);

    @Autowired
    RepeatSubmitService repeatSubmitService;

    @Autowired
    PassLoginService passLoginService;


    public  CommonResponse methodFactory(String code, String param) {
        String value = PropertiesUtil.get("apicode.properties", code);
        String[] val = value.split(":");
        String path = val[0];
        String methodStr = val[1];
        CommonResponse commonResponse = new CommonResponse();
        try {
            Class clazz = Class.forName(path);
            Method method = clazz.getMethod(methodStr, new Class[]{String.class});
            JSONObject paramJson = JSONUtil.parseObj(param);
            CommonResponse loginResponse = verifyLogin(method, paramJson);
            if (loginResponse.getCode() != 1000){
                return loginResponse;
            }
            CommonResponse submitResponse = verifyReSubmit(method, paramJson);
            if (submitResponse.getCode() != 1000){
                return submitResponse;
            }
            commonResponse = (CommonResponse) method.invoke(SpringUtil.getBean(clazz), new Object[]{param});
        } catch (ClassNotFoundException e) {
            log.error("FactoryConfig==>methodFactory", e);
        } catch (NoSuchMethodException e) {
            log.error("FactoryConfig==>methodFactory", e);
        } catch (InvocationTargetException e) {
            log.error("FactoryConfig==>InvocationTargetException", e);
        } catch (IllegalAccessException e) {
            log.error("FactoryConfig==>methodFactory", e);
        }
        return commonResponse;
    }

    public  CommonResponse verifyLogin(Method method, JSONObject paramJson){
        if (!method.isAnnotationPresent(PassLogin.class)){//若没有passLogin 注解 则验证token
            String token = paramJson.getStr("token");
            CommonResponse loginResponse = passLoginService.verifyLogin(token);
            if (loginResponse.getCode() != 1000){
                return loginResponse;
            }
        }
        return CommonResponse.success();
    }

    public  CommonResponse verifyReSubmit(Method method, JSONObject paramJson){
        if (method.isAnnotationPresent(RepeatSubmitBiz.class)){
            RepeatSubmitBiz annotation = method.getAnnotation(RepeatSubmitBiz.class);
            Long expireTime = annotation.expireTime();
            CommonResponse commonResponse = repeatSubmitService.verifyReSubmit(paramJson.getStr("mobile"), expireTime);
            return commonResponse;
        }
        return CommonResponse.success();
    }

}
