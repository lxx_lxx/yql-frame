package com.yql.biz.core.interceptor;

import com.yql.common.service.PassLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class LoginVerifyInterceptor implements HandlerInterceptor {


    @Autowired
    PassLoginService passLoginService;


    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object object) throws Exception {
        /*String token = httpServletRequest.getParameter("token");// 从 http 请求头中取出 token
        // 如果不是映射到方法直接通过
        if(!(object instanceof HandlerMethod)){
            return true;
        }
        HandlerMethod handlerMethod=(HandlerMethod)object;
        Method method=handlerMethod.getMethod();
        //检查是否有passToken注释，有则跳过认证
        if (method.isAnnotationPresent(PassLogin.class)) {
            PassLogin passToken = method.getAnnotation(PassLogin.class);
            if (passToken.required()) {
                return true;
            }
        }
        CommonResponse commonResponse = passLoginService.verifyLogin(token);
        if (commonResponse.getCode() == 1000){
            return true;
        }
        ServletUtils.renderString(httpServletResponse, JSON.marshal(commonResponse));*/
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest,
                           HttpServletResponse httpServletResponse,
                           Object o, ModelAndView modelAndView) throws Exception {

    }
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest,
                                HttpServletResponse httpServletResponse,
                                Object o, Exception e) throws Exception {

    }
}
