package com.yql.biz.core.interceptor;

import com.yql.biz.mapper.VirtualWhiteMapper;
import com.yql.common.ExceptionCodes;
import com.yql.common.json.JSON;
import com.yql.common.utils.ServletUtils;
import com.yql.common.utils.StringUtils;
import com.yql.commonextend.response.CommonResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Pattern;

@Component
public class VirtualPhoneInterceptor implements HandlerInterceptor {
    static final Logger log = LoggerFactory.getLogger(VirtualPhoneInterceptor.class);


    @Value("${virtual.phone.isUse}")
    Boolean isUse;

    @Autowired
    VirtualWhiteMapper virtualWhiteMapper;

    
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object object) throws Exception {
        try {
            if (isUse) {
                String mobile = httpServletRequest.getParameter("mobile");// 从 http 手机号字段
                if (getPhoneTypeVirtual(mobile) == 0 || virtualWhiteMapper.selectCountByMobile(mobile) > 0) {
                    return true;
                }else {
                    CommonResponse commonResponse = CommonResponse.RespWithCodeMsg(ExceptionCodes.MSG_1073.code(),ExceptionCodes.MSG_1073.msg());
                    ServletUtils.renderString(httpServletResponse, JSON.marshal(commonResponse));
                    return false;
                }
            }else {
                return true;
            }
        }catch (Exception e){
            log.error("VirtualPhoneInterceptor===>preHandle==>", e);
            CommonResponse commonResponse = CommonResponse.RespWithCodeMsg(ExceptionCodes.MSG_1074.code(),ExceptionCodes.MSG_1074.msg());
            ServletUtils.renderString(httpServletResponse, JSON.marshal(commonResponse));
            return false;
        }
    }

    /**
     * Notes:验证手机运营商号码段
     * User: Guo
     * Time: 2020/3/18 16:15
     * @param phone 手机号11位
     * @return int 1中国移动，2中国联通  3中国电信  0未知
     * 支持号码段：
     * 1、中国移动
     * 支持号段：134|135|136|137|138|139|147|148|150|151|152|157|158|159|165|172|178|182|183|184|187|188|195|198
     * 其中虚拟号段：165|170号段中的1703/1705/1706
     * 2、中国联通
     * 支持号段：130|131|132|145|146|155|156|166|167|171|175|176|185|186
     * 其中虚拟号段：167|171|170号段中的1704/1707/1708/1709
     * 3、中国电信
     * 支持号段：133|149|153|173|174|177|180|181|189|191|199
     * 其中虚拟号段：162|170号段中的1700/1701/1702
     * 其中物联网号段：149
     */
    Integer getPhoneTypeVirtual(String phone) {
        String isChinaMobile = "^170[356]\\d{7}$|^(?:165)\\d{8}$"; //移动
        String isChinaUnion = "^170[47-9]\\d{7}$|^(?:167|171)\\d{8}$"; //联通
        String isChinaTelcom = "^170[0-2]\\d{7}$|^(?:162)\\d{8}$"; //电信
        if (Pattern.matches(isChinaMobile, phone)) {
            return 1;
        } else if (Pattern.matches(isChinaUnion, phone)) {
            return 2;
        } else if (Pattern.matches(isChinaTelcom, phone)) {
            return 3;
        } else {
            return 0;
        }
    }
}
