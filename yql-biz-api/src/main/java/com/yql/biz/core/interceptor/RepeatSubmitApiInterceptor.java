package com.yql.biz.core.interceptor;

import com.yql.common.ExceptionCodes;
import com.yql.common.service.RepeatSubmitService;
import com.yql.common.annotation.RepeatSubmitBiz;
import com.yql.common.json.JSON;
import com.yql.common.utils.ServletUtils;
import com.yql.commonextend.response.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * 防止重复提交拦截器
 *
 * @author ruoyi
 */
@Component
public abstract class RepeatSubmitApiInterceptor extends HandlerInterceptorAdapter
{
    @Autowired
    RepeatSubmitService repeatSubmitService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
        /*if (handler instanceof HandlerMethod)
        {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            RepeatSubmitBiz annotation = method.getAnnotation(RepeatSubmitBiz.class);
            if (annotation != null)
            {
                Long expireTime = annotation.expireTime();
                String mobile = request.getParameter("mobile");
                if (repeatSubmitService.verifyReSubmit(mobile, expireTime).getCode() != 1000)
                {
                    CommonResponse commonResponse = CommonResponse.RespWithCodeMsg(ExceptionCodes.MSG_1075.code(), ExceptionCodes.MSG_1075.msg());
                    ServletUtils.renderString(response, JSON.marshal(commonResponse));
                    return false;
                }
            }
            return true;
        }
        else
        {
            return super.preHandle(request, response, handler);
        }*/
        return true;
    }
}
