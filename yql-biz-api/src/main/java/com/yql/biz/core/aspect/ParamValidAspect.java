package com.yql.biz.core.aspect;

import com.yql.common.core.domain.BaseEntity;
import com.yql.commonextend.utils.ParamUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
/**
 * 安全整改，切勿刪除
 */
@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
public class ParamValidAspect {

    @Pointcut("execution(* com.yql..*.*Controller.*(..))")
    @SuppressWarnings("EmptyMethod")
    public void pointCut() {
    }

    /**
     * 声明环绕通知
     *
     * @param
     * @return
     * @throws Throwable
     */
    @Around("pointCut()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        if (joinPoint.getArgs().length > 0 && ParamUtil.functionB(joinPoint.getArgs()[0])) {
            BaseEntity baseEntity = (BaseEntity) joinPoint.getArgs()[0];
            Object dataScope = baseEntity.getParams().get("dataScope");
            if (null != dataScope) {
                baseEntity.getParams().put("dataScope", "");
            }
        }

        Object obj = joinPoint.proceed();

        return obj;

    }

}
