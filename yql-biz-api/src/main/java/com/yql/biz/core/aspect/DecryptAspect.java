package com.yql.biz.core.aspect;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.yql.common.config.RuoYiConfig;
import com.yql.common.json.JSON;
import com.yql.common.json.JSONObject;
import com.yql.common.utils.DateUtils;
import com.yql.commonextend.config.TripleDesConfig;
import com.yql.commonextend.response.CommonResponse;
import com.yql.commonextend.utils.TripleDesUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Author: chenjun
 * @Description: 输出参数统一加密拦截器
 * @Date: 2021-06-29
 */
@Aspect
@Component
public class DecryptAspect {

    @Pointcut("execution(public * com.yql..*.controller..*.*(..)) && !execution(* com.yql.common.core.controller.BaseController.*(..)))")
    //切入点描述 这个是controller包的切入点
    public void resultDecrypt() {
    }
    @Around("resultDecrypt()")
    public Object doAfter(ProceedingJoinPoint pjp) throws Throwable {
        Object result = pjp.proceed();// result的值就是被拦截方法的返回值
        if (RuoYiConfig.getCrypticSwitch()) {
            if (result instanceof CommonResponse) {
                CommonResponse cr = (CommonResponse) result;
                if(cr.getData() != null) {
                    String json = JSON.marshal(cr.getData());
                    TripleDesUtils tripleDesUtils = new TripleDesUtils();
                    cr.setData(tripleDesUtils.TripleDesEncrypt(json));
                }
                return cr;
            }
        }
        return result;
    }

    public static void main(String[] args) throws Exception {
        TripleDesConfig tripleDesConfig = new TripleDesConfig();
        tripleDesConfig.setPubKey("238EC4AECBF35BC3B8AC9ACD");
        tripleDesConfig.setPubIvs("22345135");
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("openId", "1234567");
        jsonObject.put("params", jsonObject1.toCompactString());
        String result = JSON.marshal(jsonObject1);
        System.out.println(result);
        TripleDesUtils tripleDesUtils = new TripleDesUtils();
        System.out.println(tripleDesUtils.TripleDesEncrypt(result));
    }
}
