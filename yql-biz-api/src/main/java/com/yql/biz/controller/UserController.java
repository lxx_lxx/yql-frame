package com.yql.biz.controller;

import cn.hutool.core.util.ObjectUtil;
import com.jcraft.jsch.SftpException;
import com.yql.biz.domain.ThzcUser;
import com.yql.biz.domain.User;
import com.yql.biz.service.IThzcUserService;
import com.yql.biz.service.IUserService;
import com.yql.common.ExceptionCodes;
import com.yql.common.annotation.Log;
import com.yql.common.annotation.PassLogin;
import com.yql.common.annotation.RepeatSubmitBiz;
import com.yql.common.config.RuoYiConfig;
import com.yql.common.core.controller.BaseController;
import com.yql.common.enums.BusinessType;
import com.yql.common.enums.OperatorType;
import com.yql.common.exception.file.InvalidExtensionException;
import com.yql.common.utils.JwtUtils;
import com.yql.common.utils.StringUtils;
import com.yql.commonextend.config.OssConfig;
import com.yql.commonextend.config.SmsConfig;
import com.yql.commonextend.core.domin.ImageEntity;
import com.yql.commonextend.response.CommonResponse;
import com.yql.commonextend.utils.FileUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @Author: chenjun
 * @ClassName: UserController
 * @Description: 用户接口控制器
 * @Date: 2021-03-19
 * @Time: 9:19
 **/
@Api(tags = "用户接口管理")
@RestController
@RequestMapping("/app/user")
public class UserController extends BaseController{

    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Resource
    private IThzcUserService thzcUserService;

    @Resource
    private IUserService userService;

    /**
     * 用户列表 mybatis plus方式
     * @param
     * @return
     */
    @ApiOperation("用户列表")
    @PostMapping("/list_plus")
    @Log(title = "用户列表plus", operatorType = OperatorType.MOBILE, businessType = BusinessType.OTHER)
    @PassLogin
    public CommonResponse list(String mobile, String userId, Integer pageNum, Integer pageSize) {
        startPage();
        List<User> list = userService.list(mobile, userId);
        if(ObjectUtil.isNotEmpty(list)){
            return CommonResponse.successWithData(getDataTable(list));
        }
        return CommonResponse.successWithNullList();
    }

    /**
     * 用户列表 mybatis方式
     * @param thzcUser 用户姓名
     * @return
     */
    @ApiOperation("用户列表")
    @PostMapping("/list")
    @Log(title = "用户列表", operatorType = OperatorType.MOBILE, businessType = BusinessType.OTHER)
    public CommonResponse list(ThzcUser thzcUser) {
        startPage();
        List<ThzcUser> list = thzcUserService.selectThzcUserList(thzcUser);
        if(ObjectUtil.isNotEmpty(list)){
            return CommonResponse.successWithData(getDataTable(list));
        }
        return CommonResponse.successWithNullList();
    }

    /**
     * 查询用户信息
     *
     * @return
     */
    @ApiOperation("查询用户信息")
    @PostMapping("/query")
    public CommonResponse query(Integer userId) {
        ThzcUser user = thzcUserService.selectThzcUserById(userId);
        if (ObjectUtil.isNotEmpty(user)) {
            return CommonResponse.successWithData(user);
        }
        return CommonResponse.successWithNullObjOrStr();
    }

    /**
     * 保存用户信息
     * @param mobile  手机号
     * @param cUserId 银行用户标识
     * @return
     */
    @ApiOperation("保存用户信息")
    @PostMapping("/save")
    public CommonResponse save(String mobile, String cUserId) {
        if (StringUtils.isEmpty(cUserId)) {
            return CommonResponse.RespWithCodeMsg(ExceptionCodes.USER_1014.code(), ExceptionCodes.USER_1014.msg());
        }
        int count = thzcUserService.save(mobile, cUserId);
        if (count > 0) {
            return CommonResponse.success();
        }
        return CommonResponse.failed();
    }

    /**
     * 保存用户信息
     * @param mobile  手机号
     * @param cUserId 银行用户标识
     * @return
     */
    @ApiOperation("修改用户信息")
    @PostMapping("/edit")
    public CommonResponse edit(String mobile, String cUserId,String oldUrl) {
        //对象ThzcUser使用注解@Builder后
        ThzcUser user = ThzcUser.builder().mobile(mobile).cUserId(cUserId).oldUrl(oldUrl).build();
        return CommonResponse.successWithData(user);
    }

    /**
     * 上传图片示例
     * @return
     */
    @ApiOperation("上传图片示例")
    @PostMapping("/uploadImg")
    @PassLogin
    public CommonResponse uploadImg(@RequestParam("file") MultipartFile file) {
        String url = "";
        try {
            ImageEntity imageEntity = FileUtils.upload("/order", file);
            String localUrl = imageEntity.getLocalUrl();
        } catch (Exception e) {
            logger.error("uploadImg==>",e);
        }
        return CommonResponse.successWithData(url);
    }


}
