package com.yql.biz.controller;

import com.yql.biz.domain.OrderSharding;
import com.yql.biz.service.OrderShardingService;
import com.yql.common.annotation.Log;
import com.yql.common.annotation.PassLogin;
import com.yql.common.enums.BusinessType;
import com.yql.common.enums.OperatorType;
import com.yql.commonextend.response.CommonResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "订单分库分表")
@RestController
@RequestMapping("/app/orderSharding")
public class OrderShardingController {

    @Autowired
    OrderShardingService orderShardingService;

    @ApiOperation("保存")
    @PostMapping("/save")
    @Log(title = "保存信息", operatorType = OperatorType.MOBILE, businessType = BusinessType.OTHER)
    @PassLogin
    public CommonResponse save(){
        orderShardingService.save();
        return CommonResponse.success();
    }

    @ApiOperation("查询集合")
    @PostMapping("/list")
    @Log(title = "查询订单取模表", operatorType = OperatorType.MOBILE, businessType = BusinessType.OTHER)
    @PassLogin
    public CommonResponse list(){
        List<OrderSharding> list = orderShardingService.selectList();
        return CommonResponse.successWithData(list);
    }

    @ApiOperation("查询对象")
    @PostMapping("/selectOrder")
    @Log(title = "查询订单取模表", operatorType = OperatorType.MOBILE, businessType = BusinessType.OTHER)
    @PassLogin
    public CommonResponse selectOrder(){
        OrderSharding orderSharding = orderShardingService.selectOrderSharding();
        return CommonResponse.successWithData(orderSharding);
    }


    @ApiOperation("查询时间")
    @PostMapping("/listByTime")
    @Log(title = "查询订单年月表", operatorType = OperatorType.MOBILE, businessType = BusinessType.OTHER)
    @PassLogin
    public CommonResponse listByTime(){
        List<OrderSharding> list = orderShardingService.selectListByTime();
        return CommonResponse.successWithData(list);
    }
}
