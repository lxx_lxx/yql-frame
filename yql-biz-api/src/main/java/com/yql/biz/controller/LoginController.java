package com.yql.biz.controller;


import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.yql.biz.domain.SmsInfo;
import com.yql.biz.service.LoginService;
import com.yql.biz.service.SmsInfoService;
import com.yql.biz.vo.UserLoginVo;
import com.yql.common.ExceptionCodes;
import com.yql.common.core.redis.RedisCache;
import com.yql.common.utils.DateUtils;
import com.yql.commonextend.core.domin.SmsEntity;
import com.yql.commonextend.middleware.sms.Sms;
import com.yql.core.service.WechatLoginService;
import com.yql.common.annotation.RepeatSubmitBiz;
import com.yql.common.annotation.Log;
import com.yql.common.annotation.PassLogin;
import com.yql.common.enums.BusinessType;
import com.yql.common.enums.OperatorType;
import com.yql.common.utils.StringUtils;
import com.yql.commonextend.exception.CommonException;
import com.yql.commonextend.response.CommonResponse;
import com.yql.framework.manager.AsyncManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@Api(tags = "登录接口")
@RestController
@RequestMapping("/app/login")
public class LoginController {
    private static final Logger log = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    WechatLoginService wechatLoginService;

    @Autowired
    LoginService loginService;

    @ApiOperation("获取微信信息")
    @PostMapping("/getWechatInfo")
    @Log(title = "获取微信信息", operatorType = OperatorType.MOBILE, businessType = BusinessType.OTHER)
    @PassLogin
    public CommonResponse getWechatInfo(@RequestBody String jsonObject){
        JSONObject result = JSONUtil.parseObj(jsonObject);
        String wechatCode = result.getStr("wechat_code");
        Integer wechatType = result.getInt("wechat_type");
        if (StringUtils.isBlank(wechatCode)) {
            return CommonResponse.RespWithCodeMsg(-1, "code不能为空");
        }
        CommonResponse commonResponse = new CommonResponse();
        switch (wechatType){
            case 1:
                commonResponse = wechatLoginService.baseAuthorization(wechatCode);
                break;
            case 2:
                commonResponse = wechatLoginService.userinfoAuthorization(wechatCode);
                break;
            default:
                log.error("交易码错误", wechatType);
                log.error("code==：", wechatCode);
                commonResponse = CommonResponse.RespWithCodeMsg(ExceptionCodes.MSG_1068.code(), ExceptionCodes.MSG_1068.msg());
        }
        return commonResponse;
    }

    @ApiOperation("永久登录")
    @PostMapping("/getUserToken")
    @Log(title = "永久登录", operatorType = OperatorType.MOBILE, businessType = BusinessType.OTHER)
    @PassLogin
    public CommonResponse getUserToken(@RequestBody String jsonObject){
        UserLoginVo userLoginVo = JSONUtil.toBean(jsonObject, UserLoginVo.class);
        return  loginService.getUserToken(userLoginVo);
    }

    @ApiOperation("登录")
    @PostMapping("/doLogin")
    @Log(title = "登录", operatorType = OperatorType.MOBILE, businessType = BusinessType.OTHER)
    @PassLogin
    public CommonResponse doLogin(@RequestBody String jsonObject){
        UserLoginVo userLoginVo = JSONUtil.toBean(jsonObject, UserLoginVo.class);
        return  loginService.doLogin(userLoginVo);
    }


    @ApiOperation("获取图形验证码")
    @PostMapping("/getImageCode")
    @Log(title = "获取图形验证码", operatorType = OperatorType.MOBILE, businessType = BusinessType.OTHER)
    @PassLogin
    public CommonResponse getImageCode() throws Exception{
        CommonResponse commonResponse = new CommonResponse();
        commonResponse = loginService.getImageCode();

        return commonResponse;
    }



    @ApiOperation("发送短信验证码")
    @PostMapping("/sendCode")
    @RepeatSubmitBiz(expireTime = 60*1000L)
    @PassLogin
    @Log(title = "发送短信验证码", operatorType = OperatorType.MOBILE, businessType = BusinessType.OTHER)
    public CommonResponse sendCode(@RequestBody String jsonObject){
        try{
            JSONObject result = JSONUtil.parseObj(jsonObject);
            String mobile = result.getStr("mobile");
            String imageKey = result.getStr("image_key");
            String imageCode = result.getStr("image_code");
            loginService.sendMsg(mobile, imageKey, imageCode);
            return CommonResponse.successWithMsg("发送成功");
        }catch (CommonException e){
            log.error("LoginController==>sendCode==>",e);
            return CommonResponse.RespWithCodeMsg(e.getCode(), e.getMsg());
        }
    }

    @Autowired
    SmsInfoService smsInfoService;
    @ApiOperation("发送短信验证码自定义")
    @PostMapping("/sendCodeTest")
    @RepeatSubmitBiz(expireTime = 60*1000L)
    @PassLogin
    @Log(title = "发送短信验证码", operatorType = OperatorType.MOBILE, businessType = BusinessType.OTHER)
    public JSONObject sendCodeTest(){
        try{
            SmsEntity smsEntity = new SmsEntity();
            smsEntity.setContent("【湖北建行】月月领红包本月可用余额还剩1元。");
            smsEntity.setAppid("17010103");
            smsEntity.setAppKey("F7BF4647BE30BAF6B75C3817");
            smsEntity.setAppIvs("17011170");
            smsEntity.setSendUrl("http://114.55.93.94:1701/api/sms/smsSend");
            smsEntity.setMobile("16639220008");
            Integer code = Sms.sendMessage(smsEntity);
            SmsInfo smsInfo = SmsInfo.builder().mobile(smsEntity.getMobile()).content(smsEntity.getContent())
                    .status(code).createTime(DateUtils.getCurrTimeMillis()).build();
            AsyncManager.me().execute(smsInfoService.saveSmsInfo(smsInfo));

            return null;
        }catch (CommonException e){
            log.error("LoginController==>sendCode==>",e);
            return null;
        }
    }

    @ApiOperation("测试")
    @PostMapping("/test")
    @RepeatSubmitBiz(expireTime = 60*1000L)
    @PassLogin
    @Log(title = "发送短信验证码", operatorType = OperatorType.MOBILE, businessType = BusinessType.OTHER)
    public void test(){
        try{
            System.out.println("123465496874");
        }catch (CommonException e){
            log.error("LoginController==>sendCode==>",e);
        }
    }


}
