package com.yql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @Author:
 * @ClassName: YqlBizApplication
 * @Description: App业务模块启动类
 * @Date: 2021-03-11
 * @Time: 20:35
 **/
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class YqlBizApplication {

    public static void main(String[] args) {
        SpringApplication.run(YqlBizApplication.class, args);
    }

}
