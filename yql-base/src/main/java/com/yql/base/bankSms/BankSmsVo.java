package com.yql.base.bankSms;

import lombok.Data;

@Data
public class BankSmsVo {

    private String fpayHost;

    private String fpayPort;

    private String model;

    private String mobile;
}
