package com.yql.base.bankSms;


import cn.hutool.json.JSONObject;
import com.yql.base.utils.ResultUtil;
import com.yql.base.utils.SocketUtils;
import com.yql.common.core.redis.RedisCache;
import com.yql.commonextend.exception.CommonException;
import com.yql.commonextend.response.ExceptionCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class BankSms  {
    final static Logger log = LoggerFactory.getLogger(BankSms.class);

    @Autowired
    private SocketUtils socketUtils;

    @Autowired
    private RedisCache redisCache;


    private String RefNoK = "RefNo:";

    @Value("${captcha.sms.bank.fpayHost}")
    private String fpayHost;

    @Value("${captcha.sms.bank.fpayPort}")
    private String fpayPort;

    @Value("${captcha.sms.bank.model}")
    private String model;

    public String getMessage(String mobile){
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sysTime = new SimpleDateFormat("HHmmss");
        String TxDate = df.format(new Date());
        String TxTime = sysTime.format(new Date());
        String OrgNo = null;
        String TxCode = null;
        String RetCode = null;
        String RetMsg = null;
        String code = null;
        JSONObject Resp = new JSONObject();
        try {
            // 验证图形验证码
            //verifyCaptchaCode(Req.getString("uuid"),Req.getString("captchaCode"));
//            verifyCaptchaCode(uuid,captchaCode);
            JSONObject retData = new JSONObject();
//            if (org.springframework.util.StringUtils.isEmpty(mobile) || org.springframework.util.StringUtils.isEmpty(model)) {
//                RetCode = "E0009";
//                RetMsg = "短信发送失败，手机号或者模板号不能为空";
//                log.info(RetMsg);
//                Resp.put("RetCode", RetCode);
//                Resp.put("RetMsg", RetMsg);
//                String result = ResultUtil.buildResult(OrgNo, TxCode, Resp);
//                log.info(TxCode + "返回信息：", result);
//                return Result.failed(-1,result);
//            }

            String sendMsgXml = socketUtils.getSendXml(mobile, model);
            log.info(TxCode + "短信验证码请求报文：" + sendMsgXml);
            String sMsg = SocketUtils.getSmsSocket(fpayHost, fpayPort, 6, sendMsgXml);
            //String sMsg = ccbBankApiService.sendCode(Req.getString("Mobile"), model);
            log.info(TxCode + "短信验证码响应报文：", sMsg);
            if ("".equals(sMsg) || null == sMsg) {
                RetCode = "E00017";
                RetMsg = "短信发送失败，返回信息为空";
                Resp.put("RetCode", RetCode);
                Resp.put("RetMsg", RetMsg);
                String result = ResultUtil.buildResult(OrgNo, TxCode, Resp);
                log.info(TxCode + "返回信息：", result);
                throw new CommonException(new ExceptionCode(1060, "短信下发失败"));
            }
            String respXml = sMsg.substring(sMsg.indexOf("<Resp>") + 6, sMsg.indexOf("</Resp>"));
            RetCode = respXml.substring(respXml.indexOf("<RetCode>") + 9, respXml.indexOf("</RetCode>"));
            RetMsg = respXml.substring(respXml.indexOf("<RetMsg>") + 8, respXml.indexOf("</RetMsg>"));
            String refNo = respXml.substring(respXml.indexOf("<RefNo>") + 7, respXml.indexOf("</RefNo>"));
            log.info(RetMsg);
            if ("00000".equals(RetCode)) {
               code = refNo;
            }
            Resp.put("RetCode", RetCode);
            Resp.put("RetMsg", RetMsg);
        }catch (Exception e) {
            log.error("验证码发送失败", e);
            throw new CommonException(new ExceptionCode(1060, "短信下发失败"));
        }
        String result = ResultUtil.buildResult(OrgNo, TxCode, Resp);
        log.info(TxCode + "返回报文:" + result);
        return code;
    }
}
