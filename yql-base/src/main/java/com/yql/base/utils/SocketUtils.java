package com.yql.base.utils;


import com.yql.commonextend.utils.UUIDUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Configuration
@Slf4j
public class SocketUtils {


    public String getComSocket(String xml, int length, String host, String port) throws Exception {
        String sMsg = "";
        Socket socket = null;
        OutputStream oStream = null;
        InputStream iStream = null;
        try {
            socket = new Socket(host, Integer.parseInt(port));
            oStream = socket.getOutputStream();
            xml = String.format("%0" + length + "d", xml.getBytes("UTF-8").length) + xml;// xml前加length位的请求长度，不够length位前面补0
            byte[] msg = xml.getBytes("UTF-8");
            log.info("发送报文：" + xml);
            oStream.write(msg);// 发送请求
            oStream.flush();

            iStream = socket.getInputStream();
            byte[] bSize = new byte[length];
            iStream.read(bSize);
            int len = Integer.parseInt(new String(bSize, "UTF-8"));// 先读取length位转换成数字，为接下来需读取的字节长度

            byte[] bMsg = new byte[len];
            iStream.read(bMsg);// 读取返回数据
            sMsg = new String(bMsg, "UTF-8");
            log.info("返回报文:" + sMsg);
        } finally {
            if (iStream != null) {
                iStream.close();
            }
            if (oStream != null) {
                oStream.close();
            }
            if (socket != null) {
                socket.close();
            }
        }
        return sMsg;
    }

    public String getSendXml(String telephone, String busiType) {
        Date date = new Date();
        DateFormat txDateFormat = new SimpleDateFormat("yyyyMMdd");
        DateFormat txTimeFormat = new SimpleDateFormat("HHmmss");
        String txDate = txDateFormat.format(date);
        String txTime = txTimeFormat.format(date);
        String TraceNo = UUIDUtil.getUUid();
        String xml = "<?xml version=\"1.0\" encoding=\"GB18030\" ?>\r\n" +
                " <Root>\r\n" +
                "<Head>\r\n" +
                "<TxCode>S001</TxCode>\r\n" +
                "<OrgNo>105584089990004</OrgNo>\r\n" +
                "<TraceNo>" + TraceNo + "</TraceNo>\r\n" +
                "<SecNo></SecNo>\r\n" +
                "</Head>\r\n" +
                "<Req>\r\n" +
                "<TxCode>S001</TxCode>\r\n" +
                "<TxDate>" + txDate + "</TxDate>\r\n" +
                "<TxTime>" + txTime + "</TxTime>\r\n" +
                "<BusiType>" + busiType + "</BusiType>\r\n" +
                "<Telephone>" + telephone + "</Telephone>\r\n" +
                "<TraceNo>" + TraceNo + "</TraceNo>\r\n" +
                "</Req>\r\n" +
                "</Root>";
        return xml;
    }

    public String getIdentifyXml(String telephone, String refNo, String dyniCode) {
        Date date = new Date();
        DateFormat txDateFormat = new SimpleDateFormat("yyyyMMdd");
        DateFormat txTimeFormat = new SimpleDateFormat("HHmmss");
        String txDate = txDateFormat.format(date);
        String txTime = txTimeFormat.format(date);
        String TraceNo = UUIDUtil.getUUid();
        String xml = "<?xml version=\"1.0\" encoding=\"GB18030\" ?>\r\n" +
                " <Root>\r\n" +
                "<Head>\r\n" +
                "<TxCode>S002</TxCode>\r\n" +
                "<OrgNo>105584089990004</OrgNo>\r\n" +
                "<TraceNo>" + TraceNo + "</TraceNo>\r\n" +
                "<SecNo></SecNo>\r\n" +
                "</Head>\r\n" +
                "<Req>\r\n" +
                "<TxCode>S002</TxCode>\r\n" +
                "<TxDate>" + txDate + "</TxDate>\r\n" +
                "<TxTime>" + txTime + "</TxTime>\r\n" +
                "<DyniCode>" + dyniCode + "</DyniCode>\r\n" +
                "<Telephone>" + telephone + "</Telephone>\r\n" +
                "<RefNo>" + refNo + "</RefNo>\r\n" +
                "<TraceNo>" + TraceNo + "</TraceNo>\r\n" +
                "</Req>\r\n" +
                "</Root>";
        return xml;
    }

    public String getBankCardXml(String idCard, String curPg, String perNum) {
        Date date = new Date();
        DateFormat txDateFormat = new SimpleDateFormat("yyyyMMdd");
        DateFormat txTimeFormat = new SimpleDateFormat("HHmmss");
        String txDate = txDateFormat.format(date);
        String txTime = txTimeFormat.format(date);
        String TraceNo = UUIDUtil.getUUid();
        String xml = "<?xml version=\"1.0\" encoding=\"GB18030\" ?>\r\n" +
                " <Root>\r\n" +
                "<Head>\r\n" +
                "<TxCode>Z017</TxCode>\r\n" +
                "<OrgNo>105584089990004</OrgNo>\r\n" +
                "<TraceNo>" + TraceNo + "</TraceNo>\r\n" +
                "<SecNo></SecNo>\r\n" +
                "</Head>\r\n" +
                "<Req>\r\n" +
                "<TxCode>Z017</TxCode>\r\n" +
                "<TxDate>" + txDate + "</TxDate>\r\n" +
                "<TxTime>" + txTime + "</TxTime>\r\n" +
                "<CertId>" + idCard + "</CertId>\r\n" +
                "<CurPg>" + curPg + "</CurPg>\r\n" +
                "<PerNum>" + perNum + "</PerNum>\r\n" +
                "<TraceNo>" + TraceNo + "</TraceNo>\r\n" +
                "</Req>\r\n" +
                "</Root>";
        return xml;
    }

    //发送短信
    public static String getSmsSocket(String host, String port, int length, String xml) {
        Socket socket = null;
        String sMsg = "";
        try {
            socket = new Socket(host, Integer.parseInt(port));
            OutputStream oStream = socket.getOutputStream();
            xml = String.format("%0" + length + "d", xml.length()) + xml;// xml前加6位的请求长度，不够6位前面补0
            byte[] msg = xml.getBytes("GB18030");
            log.info("reqXml:" + xml);


            oStream.write(msg);// 发送请求
            oStream.flush();

            InputStream iStream = socket.getInputStream();
            byte[] bSize = new byte[length];
            iStream.read(bSize);
            int len = Integer.parseInt(new String(bSize, "GB18030"));// 先读取6位转换成数字，为接下来需读取的字节长度

            byte[] bMsg = new byte[len];
            iStream.read(bMsg);// 读取返回数据

            sMsg = new String(bMsg, "GB18030");
            log.info("respXml:" + sMsg);
        } catch (Exception e) {
            log.error("socket error");
            e.printStackTrace();
        } finally {
            try {
                if(socket!=null){
                    socket.close();
                }

            } catch (IOException e) {
                log.error("close socket error");
                e.printStackTrace();
            }
        }
        return sMsg;
    }

    public static void main(String[] args) {

    }

    public String getZ001Xml() {
        Date date = new Date();
        DateFormat txDateFormat = new SimpleDateFormat("yyyyMMdd");
        DateFormat txTimeFormat = new SimpleDateFormat("HHmmss");
        String txDate = txDateFormat.format(date);
        String txTime = txTimeFormat.format(date);
        String TraceNo = UUIDUtil.getUUid() ;
        String xml = "<?xml version=\"1.0\" encoding = \"GB18030\"?>\n" +
                "<Root>\n" +
                "<Head>\n" +
                "<TxCode>ZOO1</TxCode>\n" +
                "<OrgNo>10558408999088</OrgNo>\n" +
                "<TraceNo>" + TraceNo + "</TraceNo>\n" +
                "<SecNo>1</SecNo>" +
                "</Head>\n" +
                "      <Req>\n" +
                "<TxCode>Z001</TxCode>\n" +
                "<TxDate>" + txDate + "</TxDate>\n" +
                "<TxTime>" + txTime + "</TxTime>\n" +
                "<TraceNo>" + TraceNo + "</TraceNo>\n" +
                "       </Req>\n";
        return xml;
    }
}