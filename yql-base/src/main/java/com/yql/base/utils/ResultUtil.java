package com.yql.base.utils;


import cn.hutool.json.JSONObject;
import com.yql.commonextend.utils.UUIDUtil;


/**
 * @author
 * @version 创建时间：20200525
 * 类说明
 * 返回前台的接口数据封装类
 */
public class ResultUtil {

    static public JSONObject buildResult(String TxCode, String OrgNo, String traceNo, int secNo, String signData, String encryptReq){
        JSONObject result = new JSONObject();
        JSONObject Head = new JSONObject();
        Head.put("TxCode", TxCode);
        Head.put("OrgNo", OrgNo);
        Head.put("TraceNo", traceNo);
        Head.put("SecNo", secNo);
        Head.put("SignData", signData);
        result.put("Head", Head);
        result.put("Resp", encryptReq);
        return result;
    }
    private static int getSeqNo() {
        long l = System.currentTimeMillis();
        String s = String.valueOf(l);
        int sqe = Integer.parseInt(s.substring(s.length() - 1));
        sqe = sqe + 1;
        return sqe;
    }
    static public JSONObject buildResult(String OrgNo, String TxCode, String ApiCode, JSONObject Resp){
        JSONObject result = new JSONObject();
        JSONObject Head = new JSONObject();
        String traceNo = UUIDUtil.getUUid();
        Head.put("TxCode", TxCode);
        Head.put("OrgNo", OrgNo);
        Head.put("TraceNo", traceNo);
        Head.put("SecNo", getSeqNo());
        Head.put("SignData", "");
        result.put("Head", Head);
        result.put("Resp", Resp);
        return result;
    }


    static public String buildResult(String OrgNo,String TxCode, JSONObject Resp){
        JSONObject result = new JSONObject();
        JSONObject Head = new JSONObject();
        String SecNo = UUIDUtil.getUUidSecNo() ;
        String traceNo = UUIDUtil.getUUid();
        Head.put("TxCode", TxCode);
        Head.put("OrgNo", OrgNo);
        Head.put("TraceNo", traceNo);
        Head.put("SecNo", SecNo);
        Head.put("SignData", "");
        result.put("Head", Head);
        result.put("Resp", Resp);
        return result.toString();
    }

}
