@author 陈君
@date 2021.6.25

# 框架说明：

## 一、基础部分

本框架基于RuoYi框架二次开发，RuoYi 是一个 Java EE 企业级快速开发平台，
基于经典技术组合（Spring Boot、Apache Shiro、MyBatis、Thymeleaf、Bootstrap），
内置模块如：部门管理、角色用户、菜单及按钮授权、数据权限、系统参数、日志管理、通知公告等。
在线定时任务配置；支持集群，支持多数据源，支持分布式事务。

## 二、持久层

1、集成了MyBatis Plus，支持mybatis已有写法，具备条件构造器、代码生成器、分页插件、多种主键自动生成，单表开发更高效。
2、集成了Sharding-Jdbc，用于大数据量高并发场景的分库分表持久层写法，大大提高分库分表场景的开发效率。

## 三、拦截器

1、DecryptAspect   自定义输出参数统一加密拦截器
2、WebLogAspect    自定义对外接口日志输出拦截器
3、LogAspect       后台操作、业务日志拦截器   RuoYi框架自带，无变更  
4、RepeatSubmitInterceptor 防重复提交拦截器  RuoYi框架自带，有变更  
5、DataSourceAspect  多数据源切换拦截器      RuoYi框架自带，无变更

## 三、日志处理

1、业务日志   通过在Controller方法上加@Log注解实现
2、接口日志   WebLogAspect拦截器通用，业务代码中无需增加
3、SQL日志   mybatis的Debug日志有打印，可修改配置：logging:level:com.yql: debug
4、日志配置   logback.xml可配置日志文件输出路径、日志名称等，如out-interface.log日志文件为外部接口调用输出日志；

## 四、异常处理

1、yql-admin：全局异常yql-framework中的GlobalExceptionHandler为yql-admin的通用异常处理
2、yql-admin：当访问页面代码出现404,500，无权限时，将跳转到对应的错误页面，\resources\templates\error\xxx.html
3、yql-biz-api：全局异常yql-biz-api中的GlobalExceptionHandler重写覆盖了yql-framework的异常，只在该业务包中使用，
  该类的包名必须为com.yql，这样才能覆盖yql-framework已有的GlobalExceptionHandler
4、增加解密异常类CryptoException，在过滤器ParameterRequestFilter中捕获解密异常，如果输入参数解密失败，会返回非法操作；
5、相关异常类：com.yql.common.exception

## 五、安全校验

1、框架通过Shiro实现session管理、权限验证、防XSS等。接口访问登录认证在yql-framework的ShiroConfig中开放。
2、配置文件数据库和缓存密码通过yql-admin/src/test/java/com/yql/util/JasyptTest生成。

## 四、通用工具类

1、请求返回编码  通过CommonResponse对象返回，通用返回值在ResponseConstant中
2、日期时间     时间戳处理，在yql-commom的\com\yql\commonextend\utils中
3、加解密工具类  在yql-commom的\com\yql\commonextend\utils中
4、hutool工具类 该工具类有比较完整的各种util处理方法
5、基于Mybatis Plus的代码生成器工具，见yql-biz-api的CodeGenerator（仅涉及这一个类），可直接执行main方法生成
6、yql-commom的\com\yql\commonextend\utils\HttpUtils用于调用外部接口(含通过代理调用)，会单独打印日志到**out-interface.log文件中
注:非业务相关的util工具类都应存放在yql-commom的\com\yql\commonextend\utils中

## 五、中间件

1、Redis缓存
2、文件上传下载OSS或本地
3、RocketMq消息

## 六、业务公共工具

七、DEMO
1、UserServiceImpl，使用mybatis plus实现
2、OrderShardingServiceImpl，sharding-jdbc分库分表实现





八、其他