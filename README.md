<<<<<<< HEAD
# yql-frame

#### 介绍
入职框架练习

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
=======
@author 陈君
@date 2021.6.25

# 项目说明：

公司JAVA快速开发框架结构，封装公共Util、日志、安全、中间件等工具类，让项目开发更方便、更安全、更快速、更规范。
项目区分ruoyi与扩展的包名，便于区分框架原有代码，方便后续更新升级。
注：本项目基于RuoYi框架 http://doc.ruoyi.vip

# 公共分包说明：

yql-common      ruoyi公共common，需扩展自己公共类在commonextend目录下扩展
yql-framework   ruoyi公共framework相关类，含动态数据源、拦截器、shiro安全验证框架，需扩展自己公共类在frameworkextend目录下扩展
yql-generator   ruoyi公共代码生成工具包
yql-system      ruoyi后台管理模块包，含后台权限管理和监控模块代码
yql-base        公司业务相关基础公共包，含加解密工具类、通用异常编码、短信、虚拟号段、黑白名单等
注：上述包是为了满足公司级框架，不允许增加个性业务代码。

yql-admin       后台管理模块模板，扩展业务功能的管理后台时使用【需单独部署】
yql-biz-api     业务接口模块模板，即controller层代码      【需单独部署】
yql-quartz      后台定时任务模板，扩展定时任务时使用        
yql-biz-service 业务代码service，即service和mapper层代码 
注：yql应改为项目简称xxx，如需做应用拆分则在xxx-biz-api、xxx-biz-service后增加yyy子应用名。

# 【新项目使用步骤】：

1、复制yql-admin、yql-quartz、yql-biz-api、yql-biz-service，将yql更改为项目简称xxx，包括包名的com.yql改为com.xxx；
2、各pom.xml配置文件需把步骤1中涉及的yql改为xxx；
3、引入公共分包的私有仓库地址；暂参考该命令引入：
   mvn install:install-file -Dfile=D:\yql-common-4.6.1.jar -DgroupId=com.yql -DartifactId=yql-common -Dversion=4.6.1 -Dpackaging=jar
4、yql-biz-api中的GlobalExceptionHandler需单独创建目录com.yql.framework.web.exception包存放；
5、启动类的注解上需要补充scanBasePackages扫描的包，如下xxx为项目简称，參考如下：
   @SpringBootApplication(exclude = {DataSourceAutoConfiguration.class}, scanBasePackages = {"com.yql.*","com.xxx.*"})
6、修改mysql数据库、redis缓存等相关配置即可；
   密码相关修改采用Jasypt加密方式，找到yql-biz-api的application.yml文件的最底部配置文件加密jasypt，该配置为加密的密钥，
   通过yql-admin的test.java.com.yql.util的JasyptTest类生成密码，密码以"ENC(密码xxx)"形式填下即可。                                                         
7、其他中间件默认关闭状态，如涉及则修改其配置（后续再更新说明）。

# jwt验证使用说明

1.配置路径： com.yql.biz.core.interceptor.LoginVerifyInterceptor  以token为校验
2.在无需验证登录的接口加入@PassLogin注解即可跳过验证 例如登录接口 演示地址：com.yql.biz.controller.LoginController
3.通过token获取userId 即使用JwtUtils.getUser(token, User.class);可直接获取userId

# 防重复提交使用说明

1.yql-biz-service:com.yql.common.service.RepeatSubmitService 以手机号mobile为条件进行防重复提交,可根据自己的业务修改相关逻辑
2.在需要防重复提交接口中 加入该注解@RepeatSubmitBiz(expireTime = 间隔毫秒数) 即可实现防重复提交功能 其中参数默认是3秒间隔 也可在注解中自定义间隔毫秒数
3.演示地址：com.yql.biz.controller.UserController.repeatSubmit

sharding分库分表使用说明
1.api模块中 配置文件加上需分库分表的数据源 演示示例的数据源为 order1
2.api模块中 sharding配置分库策略均在 com.yql.biz.core.config.ShardingDataSourceConfig
3.api模块中 controller、service、 mapper 均有演示案例 按模比取值分库分表 按年月分库分表
4.common模块中 DateUtils 增加额外关于日期的方法
5.framework模块中 com.yql.framework.config.DruidConfig 增加sharding相关数据源

# 添加动态导出xls

1.com.yql.commonextend.utils.ExcelUtil  动态导出xls工具类
2.演示示例参考com.yql.web.controller.system.SysUserController.#ExcelUtilDemo 
>>>>>>> 6ce6c01 (first commit)
